#pragma once 
#include <random>
#include <glm\vec3.hpp>
#include "cuda_runtime.h"


__device__ __host__ inline float epsilion_f()
{
	return  0.00001f;
}
__device__ __host__ inline float infinity_f()
{
	return (float)0x7f800000;
}

__host__ inline float random(float a = 0.0f,float b = 1.0f)
{
	float random = ((float)rand())/(float)RAND_MAX;
	float difference = b-a;
	return a + (random * difference);
}

__host__ inline glm::vec3 random(glm::vec3 a = glm::vec3(), glm::vec3 b = glm::vec3(1,1,1))
{
	return glm::vec3(random(a.x,b.x),random(a.y,b.y),random(a.z,b.z));
}
__host__ inline float3 random(float3 a = float3(), float3 b = make_float3(1,1,1))
{
	return make_float3(random(a.x,b.x),random(a.y,b.y),random(a.z,b.z));
}
