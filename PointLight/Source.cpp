#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "PointLightDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	PointLightDemo widget;
	widget.show();
	return app.exec();
}