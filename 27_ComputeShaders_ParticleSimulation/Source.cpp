#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "ParticleSimulationDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	ParticleSimulationDemo widget;
	widget.show();
	return app.exec();
}