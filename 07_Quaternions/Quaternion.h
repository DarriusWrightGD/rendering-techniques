#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\transform.hpp>


struct Quaternion
{
	glm::vec4 versor;
	inline float dot(const glm::vec4 & q, const glm::vec4 & r)
	{
		return glm::dot(q,r);
	}

	inline glm::vec4 slerp(glm::vec4 q, glm::vec4 r, float t)
	{
		float dotProduct = dot(q,r);

		//prevents flick issue in animations...
		if(dotProduct < 0.0f)
		{
			q *= -1.0f;			
		}

		glm::vec4 result = q;
		if(fabs(dotProduct) < 1.0f)
		{
			float sinOmega = sqrt(1.0f - dotProduct * dotProduct);
			glm::vec4 result;

			if(fabs(sinOmega) <0.001f)
			{
				for (int i = 0; i < 4; i++)
				{
					result[i] = (1.0f - t) * q[i] + t * r[i];
				}
			}
			else 
			{
				float omega = acos(dotProduct);

				float a = sin((1.0f - t) * omega)/sinOmega;
				float b = sin(t * omega)/ sinOmega;

				for (int i = 0; i < 4; i++)
				{
					result[i] = q[i] * a +  r[i] * b;

				}
			}
		}
		return result;
	}
};