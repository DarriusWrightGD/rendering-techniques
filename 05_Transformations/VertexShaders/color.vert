#version 430

in vec3 vertexPoint;
in vec3 vertexColor;
uniform mat4 model;
out vec3 color;

void main () 
{
	color = vertexColor;
	gl_Position = model * vec4(vertexPoint, 1.0);
}