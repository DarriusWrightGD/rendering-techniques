#pragma once
#include <Window\OpenGLWindow.h>
#include <assimp\scene.h>
#include <Camera\FirstPersonCamera.h>
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglcolormap.h>
#include <QtGui\qopengltexture.h>
#include <map>
#include <GameObjects\GameObject.h>
#include <Components\GuiComponent.h>
#include <Components\RenderComponent.h>
#include <Components\TransformComponent.h>
// Start the concept of the model, material, and uniforms  
/*
the model knows the number of vertices that it has, also its arraybuffer 
the material will know its uniforms and the information that it points to float *
the uniform will hold the index that the uniform is located at
*/


struct Sphere
{
	glm::vec4 center;
	glm::vec4 color;
	float radius;
	glm::vec3 d;
};
struct Plane
{
	glm::vec4 normal;
};

struct Light
{
	glm::vec4 position;
};

struct UniformBlock
{
	glm::mat4 modelViewMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
};


class FragmentRayTracingDemo : public OpenGLWindow
{
public:
	FragmentRayTracingDemo(void);
	~FragmentRayTracingDemo(void);

	virtual void initialize()override;
	virtual void update()override;
	virtual void draw() override;
	virtual void resize(int width, int height)override;
	virtual void keyPressEvent(QKeyEvent * e)override;
	
private:
	void printComputeInfo();
	FirstPersonCamera camera;
	QTimer timer;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat4 modelView;
	GLfloat deltaTime;
	GLfloat totalTime;
	Program prepareProgram;
	Program traceProgram;
	Program drawProgram;

	static const uint MAX_TRACE_DEPTH = 3;
	static const uint NUM_SPHERES = 3;
	static const uint NUM_LIGHTS = 3;
	static const uint NUM_PLANES = 5;
	GLuint uniformsUBO;
	GLuint sphereUBO;
	GLuint lightUBO;
	GLuint planeUBO;
	GLuint vertexArrayBuffer;
	GLuint frameBuffers[MAX_TRACE_DEPTH];
	GLuint positionTex[MAX_TRACE_DEPTH];
	GLuint reflectedTex[MAX_TRACE_DEPTH];
	GLuint refractionTex[MAX_TRACE_DEPTH];
	GLuint reflectionIntesityTex[MAX_TRACE_DEPTH];
	GLuint refractionIntensityTex[MAX_TRACE_DEPTH];
	GLuint fullImage;


};