#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "DirectionalLightDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	DirectionalLightDemo widget;
	widget.show();
	return app.exec();
}