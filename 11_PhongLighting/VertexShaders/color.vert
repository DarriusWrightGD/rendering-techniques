#version 430 core

in vec3 vertexPoint;
//in vec3 vertexColor;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
out vec3 color;

void main () 
{
	color = vec3(0.2,0.3,0.4);
	gl_Position = projection * view * model * vec4(vertexPoint, 1.0);
}