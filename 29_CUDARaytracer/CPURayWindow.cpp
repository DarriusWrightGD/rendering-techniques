#include "CPURayWindow.h"


CPURayWindow::CPURayWindow(int width, int height) : width(width), height(height)
{
	layout = new QHBoxLayout();
	layout->addWidget(&label);
	setLayout(layout);
	sphere.color = glm::vec3(0.5f,0.8f,0.1f);
	sphere.radius = 40.0f;
	sphere.position = glm::vec3(0.0f,0.0f,-50.0f);
	setWindowTitle("CPU Ray Tracer");
	update();
}


CPURayWindow::~CPURayWindow(void)
{
}

void CPURayWindow::keyReleaseEvent(QKeyEvent * e)
{
	if(e->key() == Qt::Key::Key_Space)
	{
		update();
	}
}

void CPURayWindow::update()
{
	unsigned char * pixels = new unsigned char[width * height * 4];
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{

			glm::vec3 pixelPoint = glm::vec3((float)(x-width/2),(float)(y-height/2),100);
			float n;
			float t ;//= sphere.hit(pixelPoint.x,pixelPoint.y, &n);
			float r = 0, g = 0, b = 0;

			if(t  > -1000000.0f)
			{
				r= sphere.color.x;
				g= sphere.color.y;
				b= sphere.color.z;
			}

			int pixelLocation = (y * width + x) * 4;
			pixels[pixelLocation]   = (int)(r*255);
			pixels[pixelLocation+1] = (int)(g*255);
			pixels[pixelLocation+2] = (int)(b*255);
			pixels[pixelLocation+3] = 255;
		}
	}
	QImage image(pixels,width,height,QImage::Format::Format_RGBA8888);
	image = image.mirrored();
	label.setPixmap(QPixmap::fromImage(image));
	delete [] pixels;
}