#include "GameTimer.h"


GameTimer::GameTimer(void) : elapsedTime(0.0f)
{
	deltaTimer.start();
}


GameTimer::~GameTimer(void)
{
}


float GameTimer::delta()
{
	return  1.0f/elapsedTime;
}

void GameTimer::restart()
{
	deltaTimer.start();
}

void GameTimer::stop()
{
	elapsedTime = deltaTimer.elapsed();
}
