#pragma once


#include <QtGui\qopengl.h>
enum UniformType
{
	MAT4,
	MAT3,
	VEC3,
	VEC4,
	FLT
};

struct Uniform
{
	Uniform(GLint id, UniformType type) : id(id), type(type)
	{

	}
	
	bool operator<(const Uniform& rhs)const{return id < rhs.id; }
	bool operator>(const Uniform& rhs)const{return id > rhs.id; }
	bool operator==(const Uniform& rhs)const{return id == rhs.id; }
	GLint getId()const{ return id;}
	GLint getType()const{return type;}

private:
	GLint id;
	UniformType type;

};