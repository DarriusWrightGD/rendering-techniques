#pragma once
#include <QtGui\qopenglfunctions_4_5_compatibility.h>
#include <QtGui\qopenglfunctions_2_0.h>
#include <QtOpenGL\qglfunctions.h>
#include <QtCore\qdebug.h>
#define GL GLContext::getInstance()

class GLContext : public QOpenGLFunctions_4_5_Compatibility
{
public:
	virtual ~GLContext(void)
	{

	}
	static GLContext * getInstance()
	{
		static GLContext * instance;
		static bool initialized  = false;
		if(!initialized)
		{
			instance = new GLContext();
			instance->initializeOpenGLFunctions();
		}

		return instance;
	}

	void checkError(const char * log)
	{	
		GLenum error= GL->glGetError();
		if(error != GL_NO_ERROR)
		{
			while(error!=GL_NO_ERROR) {
				std::string errorString;

				switch(error) {
				case GL_INVALID_OPERATION:      errorString="INVALID_OPERATION";      break;
				case GL_INVALID_ENUM:           errorString="INVALID_ENUM";           break;
				case GL_INVALID_VALUE:          errorString="INVALID_VALUE";          break;
				case GL_OUT_OF_MEMORY:          errorString="OUT_OF_MEMORY";          break;
				case GL_INVALID_FRAMEBUFFER_OPERATION:  errorString="INVALID_FRAMEBUFFER_OPERATION";  break;
				}

				qDebug() << "GL_" << errorString.c_str() << log ;//<<" - "<<file<<":"<<line<<endl;
				error=GL->glGetError();
			}
		}
	}
private:
	GLContext(void){}
};

