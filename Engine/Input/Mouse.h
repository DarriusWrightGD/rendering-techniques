#pragma once
#include <glm\vec2.hpp>
#include <Export.h>

class Mouse
{
public:
	ENGINE_SHARED Mouse(void);
	ENGINE_SHARED ~Mouse(void);
	ENGINE_SHARED void setPosition(glm::vec2 position);
	ENGINE_SHARED const glm::vec2 & getPosition()const;
	ENGINE_SHARED const glm::vec2 & getDelta()const;
	ENGINE_SHARED bool isRightButtonDown()const;
	ENGINE_SHARED bool isLeftButtonDown()const;
	ENGINE_SHARED void setRightButtonDown(bool isDown);
	ENGINE_SHARED void setLeftButtonDown(bool isDown);
	ENGINE_SHARED bool moved();
private:
	glm::vec2 position;
	glm::vec2 delta;
	bool rightButtonDown;
	bool leftButtonDown;
};

