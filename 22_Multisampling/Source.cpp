#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "MultiSamplingDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	MultiSamplingDemo widget;
	widget.show();
	return app.exec();
}