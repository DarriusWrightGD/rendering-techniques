#pragma once
#include <QtWidgets\qopenglwidget.h>
#include <QtGui\qopenglfunctions_4_3_core.h>
class OpenGLWindow :  public QOpenGLWidget , protected QOpenGLFunctions_4_3_Core
{
public:
	OpenGLWindow(void);
	~OpenGLWindow(void);

protected:
	void initializeGL()override;
	void paintGL()override;
	void resizeGL(int width, int height)override;

private:
	void setUpFormat();

};

