#pragma once
#include <glm\vec2.hpp>
class Mouse
{
public:
	Mouse(void);
	~Mouse(void);
	void setPosition(glm::vec2 position);
	const glm::vec2 & getPosition()const;
	const glm::vec2 & getDelta()const;
	bool isRightButtonDown()const;
	bool isLeftButtonDown()const;
	void setRightButtonDown(bool isDown);
	void setLeftButtonDown(bool isDown);
private:
	glm::vec2 position;
	glm::vec2 delta;
	bool rightButtonDown;
	bool leftButtonDown;
};

