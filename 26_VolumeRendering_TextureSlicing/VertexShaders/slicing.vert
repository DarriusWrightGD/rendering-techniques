#version 430

layout (location = 0) in vec3 vertexPosition;

smooth out vec3 uv;
uniform mat4 MVP;

void main()
{
	gl_Position = MVP * vec4(vertexPosition,1.0);
	uv = vertexPosition + vec3(0.5);
}
