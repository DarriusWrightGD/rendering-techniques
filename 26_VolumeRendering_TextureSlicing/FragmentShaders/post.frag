#version 430
uniform sampler2D imageTexture;

in vec2 textureCoord;
out vec4 fragColor;

const vec3 lum = vec3(0.2126, 0.7152, 0.0722);

float luminance(vec3 color)
{
	return dot(lum,color);
}

subroutine vec4 PostProcessEffect();

subroutine (PostProcessEffect) vec4 edgeDetection()
{
	ivec2 pix = ivec2(gl_FragCoord.xy);
	float s00 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(-1,1)).rgb);
	float s10 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(-1,0)).rgb);
	float s20 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(-1,-1)).rgb);
	float s01 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(0,1)).rgb);
	float s21 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(0,-1)).rgb);
	float s02 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(1,1)).rgb);
	float s12 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(1,0)).rgb);
	float s22 = luminance(texelFetchOffset(imageTexture, pix, 0,ivec2(1,-1)).rgb);

	float sx = s00 + 2 * s10 + s20 - (s02 + 2 * s12 + s22);
	float sy = s00 + 2 * s01 + s02 - (s20 + 2 * s21 + s22);

	float g = sx * sx + sy * sy;
	if(g > 0.1f)return vec4(1.0);
	else return vec4(vec3(0.0),1.0);
}
subroutine uniform PostProcessEffect postEffect;

void main()
{
	fragColor = postEffect();//texture(imageTexture,textureCoord);
}