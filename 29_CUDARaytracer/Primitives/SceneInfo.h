#pragma once

#include <cuda_runtime.h>

struct SceneInfo
{

	__device__ int getSamples()
	{
		return sampleHeight * sampleWidth;
	}

	__device__ int getSampleWidth()
	{
		return sampleWidth;
	}

	__device__ int getSampleHeight()
	{
		return sampleHeight;
	}

	void setSampleHeight(int height)
	{
		sampleHeight = height;
		setSamples();
	}


	void setSampleWidth(int width)
	{
		sampleWidth = width;
		setSamples();
	}

	int width;
	int height;
	int numberOfSpheres;
	int channels;
private:
	void setSamples(){samples = sampleHeight * sampleWidth;}

	int sampleWidth;
	int sampleHeight;
	int samples;

};