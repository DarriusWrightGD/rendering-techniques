#include "FirstPersonCamera.h"
#include <Time\GameTimer.h>
#include <glm\gtx\transform.hpp>
#include <glm\gtx\rotate_vector.hpp>
#include <glm\gtx\quaternion.hpp>
#include <QtCore\qdebug.h>

const float FirstPersonCamera::DEFAULT_ROTATION_RATE = glm::radians(1.0f);
const float FirstPersonCamera::DEFAULT_MOVEMENT_RATE = 1.0f;
const float FirstPersonCamera::DEFAULT_MOUSE_SENSITIVITY = 0.1f;

FirstPersonCamera::FirstPersonCamera(float aspectRatio,float fieldOfView,  float nearPlaneDistance, float farPlaneDistance):
	Camera(aspectRatio,fieldOfView,nearPlaneDistance,farPlaneDistance), keyboard(nullptr), mouse(nullptr), movementRate(DEFAULT_MOVEMENT_RATE), rotateRate(DEFAULT_ROTATION_RATE),
	mouseSensitivity(DEFAULT_MOUSE_SENSITIVITY), pitch(0.0f), yaw(0.0f)
{
}


FirstPersonCamera::~FirstPersonCamera(void)
{
}

void FirstPersonCamera::setMouse(Mouse & mouse)
{
	this->mouse = &mouse;
}
void FirstPersonCamera::setKeyboard(Keyboard & keyboard)
{
	this->keyboard = &keyboard;
}
void FirstPersonCamera::initialize()
{
	Camera::initialize();
}
void FirstPersonCamera::update()
{
	handleKeyInput();
	handleMouseInput();
	Camera::update();
}

void FirstPersonCamera::handleKeyInput()
{
	glm::vec3 movementAmount;

	if(keyboard != nullptr)
	{
		if(keyboard->keyPressed('W'))movementAmount.y = 1.0f;
		if(keyboard->keyPressed('S'))movementAmount.y = -1.0f;
		if(keyboard->keyPressed('A'))movementAmount.x= -0.1f;
		if(keyboard->keyPressed('D'))movementAmount.x = 0.1f;
		if(keyboard->keyPressed('R'))movementAmount.z= -1.0f;
		if(keyboard->keyPressed('F'))movementAmount.z = 1.0f;
	}

	float elapsedTime = GAME_TIMER.delta();

	movementAmount *= movementRate * elapsedTime;
	glm::vec3 strafe = right * movementAmount.x;
	position += strafe;
	glm::vec3 forward = getDirection() * movementAmount.y;
	position += forward;
	position.z += movementAmount.z * elapsedTime * movementRate;

}

void FirstPersonCamera::handleMouseInput()
{
	glm::vec2 rotationAmount;

	if(mouse != nullptr && mouse->isLeftButtonDown())
	{
		rotationAmount = glm::vec2(mouse->getDelta()) * mouseSensitivity * GAME_TIMER.delta();
	}
	pitch = rotationAmount.y;
	yaw =rotationAmount.x;



	glm::vec3 cameraDirection = getDirection();
	glm::vec3 axis = glm::cross(getDirection(), up);
	
	glm::quat pitchRotation = glm::angleAxis(pitch, axis);
	glm::quat yawRotation = glm::angleAxis(yaw, up);
	glm::quat resultRotation = glm::normalize(glm::cross(pitchRotation,yawRotation));
	cameraDirection = glm::rotate(resultRotation, cameraDirection);
	lookAt = position + cameraDirection;

	//float elapsedTime = GAME_TIMER.delta();
	//rotationAmount *= rotateRate * elapsedTime;

	//glm::mat4 pitch = glm::toMat4(glm::angleAxis(rotationAmount.y,right));
	//glm::mat4 yaw = glm::rotate(rotationAmount.x, glm::vec3(0.0f,1.0f,0.0f));
	//glm::mat3 rotation = glm::mat3(pitch) * glm::mat3(yaw);
	applyRotation(glm::toMat3(resultRotation));
}