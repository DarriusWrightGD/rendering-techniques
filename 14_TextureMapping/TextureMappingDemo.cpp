#include "TextureMappingDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <stdlib.h>
#include <assimp\Importer.hpp>
#include <GLContext.h>

TextureMappingDemo::TextureMappingDemo(void): speed(0.1f), lastPosition(0.0f), lightPosition(glm::vec4(glm::normalize(glm::vec3(.2,.2,-1)),0.0f)),
	lightColor(0.4f,0.4f,0.1f),diffuse(0.2f,0.2f,0.9f,1.0f), specular(0.9f,0.0f,0.0f, 10.0f), ambientColor(0.3f,0.7,0.2), camera((float)width()/height())
{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);
	
}


TextureMappingDemo::~TextureMappingDemo(void)
{
}

void TextureMappingDemo::loadModel(const char * modelName)
{

	const char * filename = modelName;

	//Assimp::Importer importer;
	//const aiScene * scene = importer.ReadFile(filename, flags);


	const aiScene * scene = aiImportFile(filename,aiProcess_Triangulate);
	if(!scene)
	{
		fprintf(stderr, "Error: reading mesh %s\n",filename );
		exit(-1);
	}
	printSceneInfo(scene);

	const aiMesh * mesh = scene->mMeshes[0];
	printf("\t%i vertices in mesh[0]\n", mesh->mNumVertices);
	numberOfVertices = mesh->mNumVertices;

	glm::vec3 * points = nullptr;
	glm::vec3 * normals = nullptr;
	glm::vec2 * texcoords = nullptr;

	if(mesh->HasPositions())
	{
		points = new glm::vec3[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mVertices[i];
			points[i] = glm::vec3(vertexPoint.x, vertexPoint.y,vertexPoint.z);
		}

		GLuint vbo;
		GL->glGenBuffers(1, &vbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, vbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices, points, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
		GL->glEnableVertexAttribArray(0);

		delete [] points;
	}

	if(mesh->HasNormals())
	{
		normals = new glm::vec3[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D normalPoint = mesh->mNormals[i];
			normals[i] = glm::vec3(normalPoint.x, normalPoint.y,normalPoint.z);
		}

		GLuint nbo;
		GL->glGenBuffers(1, &nbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, nbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices , normals, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,NULL);
		
		GL->glEnableVertexAttribArray(1);

		delete [] normals;
	}

	if(mesh->HasTextureCoords(0))
	{
		texcoords = new glm::vec2[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mTextureCoords[0][i];
			texcoords[i] = glm::vec2(vertexPoint.x, vertexPoint.y);
		}

		GLuint vbo;
		GL->glGenBuffers(1, &vbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, vbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * numberOfVertices, texcoords, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,0,NULL);
		GL->glEnableVertexAttribArray(2);

		delete [] texcoords;
	}
}

void TextureMappingDemo::printSceneInfo(const aiScene * scene)
{
	printf("\t%i animations : \n", scene->mNumAnimations);
	printf("\t%i cameras : \n", scene->mNumCameras);
	printf("\t%i lights : \n", scene->mNumLights);
	printf("\t%i materials : \n", scene->mNumMaterials);
	printf("\t%i meshs : \n", scene->mNumMeshes);
	printf("\t%i textures : \n", scene ->mNumTextures);
}
void TextureMappingDemo::compileShader(GLuint & shader, const char * source)
{
	int params = -1;
	GL->glShaderSource(shader, 1, &source, NULL);
	GL->glCompileShader(shader);
	GL->glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}
}

void TextureMappingDemo::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	GL->glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}

void TextureMappingDemo::readShaderProgram()
{
	GLuint vertexShader = GL->glCreateShader(GL_VERTEX_SHADER);

	compileShader(vertexShader,fileToString("VertexShaders/light.vert").c_str());

	GLuint fragmentShader = GL->glCreateShader(GL_FRAGMENT_SHADER);
	compileShader(fragmentShader, fileToString("FragmentShaders/light.frag").c_str());

	shaderProgram = GL->glCreateProgram();
	GL->glAttachShader(shaderProgram, fragmentShader);
	GL->glAttachShader(shaderProgram,vertexShader);

	GL->glLinkProgram(shaderProgram);

	int params = -1;

	GL->glGetProgramiv(shaderProgram , GL_LINK_STATUS, &params);
	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : could not link shader program GL index %u\n", shaderProgram);
		printShaderInfo(shaderProgram);
		system("pause");
		exit(-1);
	}
	modelLocation = GL->glGetUniformLocation(shaderProgram, "model");
	viewLocation = GL->glGetUniformLocation(shaderProgram, "view");
	modelViewLocation = GL->glGetUniformLocation(shaderProgram, "modelView");
	projectionLocation = GL->glGetUniformLocation(shaderProgram, "projection");
	lightColorLocation = GL->glGetUniformLocation(shaderProgram, "light.color");
	modelDiffuseColorLocation = GL->glGetUniformLocation(shaderProgram, "material.diffuse");
	modelSpecularColorLocation = GL->glGetUniformLocation(shaderProgram, "material.specular");
	ambientColorLocation = GL->glGetUniformLocation(shaderProgram,"ambient");
	normalMatrixLocation = GL->glGetUniformLocation(shaderProgram,"normalMatrix");
	lightDirectionLocation = GL->glGetUniformLocation(shaderProgram, "light.position");
	textureLocation = GL->glGetUniformLocation(shaderProgram,  "basicTexture");

	modelMatrix = glm::translate(glm::vec3(0.5f,0.0f,0.0f));
	GL->glUseProgram(shaderProgram);
	//glUniform1i(textureLocation,0);
}

std::string TextureMappingDemo::fileToString(const char * filename)
{
	return std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>());
}

void TextureMappingDemo::initialize()
{
	GL->glGenVertexArrays(1,&vao);
	GL->glBindVertexArray(vao); 
	loadModel("../Models/cube.obj");
	readShaderProgram();
	loadImage("../Images/brick.png");

}

void TextureMappingDemo::loadImage(const char * filename)
{
	QImage textureImage = QGLWidget::convertToGLFormat(QImage(filename));
	
	GL->glGenTextures(1,&texId);
	GL->glActiveTexture(GL_TEXTURE0);
	GL->glBindTexture(GL_TEXTURE_2D,texId);
	GL->glTexImage2D(GL_TEXTURE_2D, 0 , GL_RGBA, 
		textureImage.width(), textureImage.height(),0,
		GL_RGBA,GL_UNSIGNED_BYTE,textureImage.bits());
	int w = textureImage.width();
	int h = textureImage.height();

	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);;
}

void TextureMappingDemo::update()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();
	GL->glUseProgram(shaderProgram);
	GL->glActiveTexture(GL_TEXTURE0);
	GL->glBindTexture(GL_TEXTURE_2D, texId);
	//glUniform1i(textureLocation,0);

	const float lightSpeed = 2.0f;

	if(GetAsyncKeyState('J'))
	{
		lightPosition.x -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('L'))
	{
		lightPosition.x += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('P'))
	{
		lightPosition.z -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('O'))
	{
		lightPosition.z += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('K'))
	{
		lightPosition.y -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('I'))
	{
		lightPosition.y += lightSpeed * GAME_TIMER.delta();
	}

	view = camera.getViewMatrix();
	camera.update();

	lightPosition = glm::vec4(glm::normalize(glm::vec3(lightPosition)),0.0);

	modelMatrix = glm::translate(glm::vec3(lastPosition,0.0f,-5.0f));
	modelView = view * modelMatrix;
	normalMatrix = glm::inverse(glm::transpose(glm::mat3(modelView)));
	GL->glUniformMatrix4fv(modelLocation,1,false, &modelMatrix[0][0]);
	GL->glUniformMatrix4fv(modelViewLocation, 1, false, &modelView[0][0]);
	GL->glUniformMatrix4fv(viewLocation,1,false, &view[0][0]);
	GL->glUniformMatrix4fv(projectionLocation,1,false, &projection[0][0]);
	GL->glUniformMatrix4fv(normalMatrixLocation,1,false, &normalMatrix[0][0]);
	GL->glUniform4fv(lightDirectionLocation,1,&lightPosition[0]);
	GL->glUniform3fv(lightColorLocation,1,&lightColor[0]);
	GL->glUniform4fv(modelDiffuseColorLocation,1,&diffuse[0]);
	GL->glUniform4fv(modelSpecularColorLocation,1,&specular[0]);
	GL->glUniform3fv(ambientColorLocation,1,&ambientColor[0]);
	repaint();
}
void TextureMappingDemo::draw() 
{
	GAME_TIMER.restart();
	GL->glBindVertexArray(vao);
	GL->glDrawArrays(GL_TRIANGLES,0,numberOfVertices);
}
void TextureMappingDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}