#include "Panel.h"


Panel::Panel(QQmlEngine* engine, const char* qmlFile) : engine(engine),
	component(engine,QUrl(qmlFile)), qmlFile(qmlFile)
{
	view = new QQuickView();
	container = createWindowContainer(view, this);
	view->setSource(QUrl(qmlFile));
	quickItem = view->rootObject();
	mainLayout = new QHBoxLayout();
	setLayout(mainLayout);
	mainLayout->addWidget(container);
}

Panel::~Panel(void)
{
}
