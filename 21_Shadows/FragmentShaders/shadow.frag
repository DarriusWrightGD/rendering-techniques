#version 430

in vec2 textureCoord;
in vec3 normal;
in vec3 position;
in vec4 shadowTex;
uniform sampler2D depthMap;
float epsilon = 0.0042;

uniform vec3 ambient;

struct Light
{
	vec3 color;
	vec4 position;
};

struct Material
{
	vec4 diffuse;
	vec4 specular;
};
uniform Light light;
uniform Material material;

out vec4 color;


float shadowValue(vec2 texCoords)
{
	float shadow = texture(depthMap, texCoords).r;
	if(shadow + epsilon < shadowTex.z)
	{
		return 0.2;
	}
	return 1.0;
}
///
vec4 ads()
{
	vec3 direction = normalize((light.position.w == 0) ? vec3(light.position) : vec3(light.position) - position);
	vec3 lightVector = normalize(vec3(-position));
	vec3 reflectVector = reflect(-lightVector, normal);
	return vec4(light.color  * (ambient + 
	vec3( material.diffuse) * max(dot(direction, normal),0.0) //+
	// vec3(material.specular) * pow(max(dot(reflectVector, lightVector),0.0),material.specular.w)
	)
	,material.diffuse.w);
}

void main()
{
	float shadowFactor = shadowValue(shadowTex.xy);
	color = vec4(ads().xyz * shadowFactor, 1.0);
}