﻿#include "RenderApp.h"
#include <QtWidgets/qmenubar.h>
#include <QtWidgets/qfiledialog.h>
RenderApp::RenderApp() : engine(),gameObjectPanel(&engine)
{
	setUpLayout();
}

void RenderApp::setUpLayout()
{
	mainWidget = new QWidget();
	mainLayout = new QHBoxLayout();
	mainWidget->setLayout(mainLayout);
	mainLayout->addWidget(&gameObjectPanel);
	mainLayout->addWidget(&glWindow);

	setCentralWidget(mainWidget);

	setUpMenu();
}

void RenderApp::setUpMenu()
{
	fileMenu = menuBar()->addMenu(tr("&File"));
	addMenu = menuBar()->addMenu(tr("&Add"));
	
	addModelAction = new QAction(tr("&Add Model"),this);
	addModelAction->setStatusTip(tr("Add a model via a file"));
	connect(addModelAction,&QAction::triggered,this,&RenderApp::showAddModelDialog);
	addMenu->addAction(addModelAction);
}

void RenderApp::showAddModelDialog()
{
	QString filename = QFileDialog::getOpenFileName(this,tr("Open Model"));
	if(!filename.isEmpty())
	{
		glWindow.addModel(filename.toStdString());
	}
}