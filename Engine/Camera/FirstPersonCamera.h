#pragma once

#include "Camera.h"
#include <Windows.h>
#include <Input\Mouse.h>
#include <Input\Keyboard.h>
#include <Export.h>

class FirstPersonCamera : public Camera
{
public:
	ENGINE_SHARED FirstPersonCamera(float aspectRatio,float fieldOfView = DEFAULT_FIELD_OF_VIEW,  float nearPlaneDistance = DEFAULT_NEAR_PLANE_DISTANCE, float farPlaneDistance = DEFAULT_FAR_PLANE_DISTANCE);
	ENGINE_SHARED virtual ~FirstPersonCamera(void);
	ENGINE_SHARED void setMouse(Mouse & mouse);
	ENGINE_SHARED void setKeyboard(Keyboard & keyboard);
	ENGINE_SHARED virtual void initialize()override;
	ENGINE_SHARED virtual void update()override;

	static const float DEFAULT_ROTATION_RATE;
	static const float DEFAULT_MOVEMENT_RATE;
	static const float DEFAULT_MOUSE_SENSITIVITY;
protected:
	virtual void handleKeyInput();
	virtual void handleMouseInput();
	Mouse * mouse;
	Keyboard * keyboard;
	float mouseSensitivity;
	float rotateRate;
	float movementRate;
	float pitch;
	float yaw;
};

