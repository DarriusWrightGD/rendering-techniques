#ifndef EXPORT_H
#define EXPORT_H

#ifdef	ENGINE_EXPORTS
#define ENGINE_SHARED __declspec( dllexport )
#else
#define ENGINE_SHARED __declspec( dllimport )
#endif

#endif // EXPORT_H