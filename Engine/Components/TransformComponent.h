#pragma once

#include <Components\Component.h>
#include <glm\glm.hpp>
class GameObject;
class TransformComponent : public Component
{
	RTTI_DECLARATIONS(TransformComponent,Component);
public:
	ENGINE_SHARED TransformComponent(GameObject * gameObject);
	ENGINE_SHARED ~TransformComponent(void);
	ENGINE_SHARED glm::mat4 & getTransform();
	ENGINE_SHARED glm::vec3 & getPosition();	
	ENGINE_SHARED glm::vec3 & getScale();
	ENGINE_SHARED glm::vec3 & getRotation();

	ENGINE_SHARED  void setPosition(const glm::vec3 & position);	
	ENGINE_SHARED  void setScale( const glm::vec3 & scale);
	ENGINE_SHARED  void setRotation(const glm::vec3 & rotation);



private:
	void calculateTransform();
	glm::mat4 modelMatrix;
	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotation;


};

