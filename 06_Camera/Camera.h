#include <glm/gtx/transform.hpp>
#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>


class Camera
{
public :
	
	Camera(float aspectRatio,float fieldOfView = DEFAULT_FIELD_OF_VIEW,  float nearPlaneDistance = DEFAULT_NEAR_PLANE_DISTANCE, float farPlaneDistance = DEFAULT_FAR_PLANE_DISTANCE);
	virtual ~Camera();
	const glm::vec3 & getPosition()const;
	glm::vec3 getDirection()const;
	const glm::vec3 & getLookAt()const;
	const glm::vec3 & getUp()const;
	const glm::vec3 & getRight()const;
	float getAspectRatio()const;
	float getFieldOfView()const;
	float getFarPlaneDifference()const;

	const glm::mat4 & getViewMatrix()const;
	const glm::mat4 & getProjectionMatrix()const;
	const glm::mat4 & getViewProjectionMatrix()const;

	void setAspectRatio(float aspectRatio);
	virtual void setPosition(glm::vec3 position);
	virtual void initialize();

	virtual void update();
	virtual void updateViewMatrix();
	virtual void updateProjectionMatrix();
	virtual void reset();
	void applyRotation(glm::mat3 rotationMatrix);

	static const float DEFAULT_FIELD_OF_VIEW;
	static const float DEFAULT_ASPECT_RATIO;
	static const float DEFAULT_NEAR_PLANE_DISTANCE;
	static const float DEFAULT_FAR_PLANE_DISTANCE;

protected:
	float fieldOfView;
	float aspectRatio;
	float nearPlaneDistance;
	float farPlaneDistance;

	glm::vec3 position;
	glm::vec3 lookAt;
	glm::vec3 up;
	glm::vec3 right;

	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	//stop the copy constructor madness
private:
	//Camera(const Camera & rhs);
	//Camera & operator=(const Camera & rhs);
};

//class Camera
//{
//
//public:
//	Camera(glm::vec3 position,float speed, float yaw , float rotationSpeed);
//	Camera();
//	void update();
//	glm::mat4 getViewMatrix()
//	{
//		glm::mat4 translation = glm::translate(-position);
//		glm::mat4 rotation = glm::rotate(-yaw, glm::vec3(0.0f,1.0f,0.0f));
//		return rotation * translation;
//	}
//
//	void setSpeed(float speed);
//	void setYaw(float yaw);
//	void setPosition(glm::vec3 position);
//
//private:
//	void checkKeys();
//	glm::vec3 position;
//	float speed;
//	float yaw;
//	float rotationSpeed;
//};

//#pragma once
//#include <glm/glm.hpp>
//#include <glm\gtx\transform.hpp>
//
//#pragma warning(disable : 4512)
//class Camera
//{
//
//
//
//	glm::vec2 oldMousePosition;
//	static const float CAMERA_SPEED;
//	static const float MOVEMENT_SPEED;
//	const glm::vec3 UP;
//
//public:
//	 Camera(void);
//	 ~Camera(void);
//	glm::vec3 position;
//	glm::vec3 viewDirection;
//	void mouseUpdate(const glm::vec2 & newMousePosition);
//
//	void moveForwards();
//	void moveBackwards();
//	void moveLeft();
//	void moveRight();
//	void moveUp();
//	void moveDown();
//
//	inline glm::mat4 getWorldToViewMatrix()const
//	{
//		return glm::lookAt(position,position+ viewDirection,UP);
//	}
//};
