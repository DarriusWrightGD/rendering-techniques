#pragma once
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglfunctions.h>
#include <QtWidgets\qopenglwidget.h>
#include <QtOpenGLExtensions\qopenglextensions.h>
#include <QtGui\qopenglfunctions_4_3_core.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <QtCore\qtimer.h>

#include <Camera\FirstPersonCamera.h>
#include <QtGui\qevent.h>
#include <Time\GameTimer.h>
#include <Input\Mouse.h>
#include <Input\Keyboard.h>

class OpenGLWindow :  public QOpenGLWidget 
{
public:
	ENGINE_SHARED OpenGLWindow(void);
	ENGINE_SHARED virtual ~OpenGLWindow(void);
	ENGINE_SHARED virtual void initialize() = 0;
	ENGINE_SHARED virtual void update() = 0;
	ENGINE_SHARED virtual void draw() = 0;
	ENGINE_SHARED virtual void resize(int width, int height) = 0;

protected:
	ENGINE_SHARED void initializeGL()override;
	ENGINE_SHARED void paintGL()override;
	ENGINE_SHARED void resizeGL(int width, int height)override;
	ENGINE_SHARED void glUpdate();
	ENGINE_SHARED void mouseMoveEvent(QMouseEvent * e)override;
	ENGINE_SHARED void mousePressEvent(QMouseEvent * e)override;
	ENGINE_SHARED void mouseReleaseEvent(QMouseEvent * e)override;
	ENGINE_SHARED virtual void setUpFormat();
	ENGINE_SHARED float aspectRatio()const;

	Mouse mouse;
	Keyboard keyboard;

private:
	QTimer timer;
};

