#pragma once
#include <vector>
#include <glm\vec3.hpp>
#include <RTTI.h> 

class Component;
class TransformComponent;
class GameObject : public RTTI
{
	RTTI_DECLARATIONS(GameObject, RTTI)
public:
	ENGINE_SHARED GameObject();
	ENGINE_SHARED virtual ~GameObject(void);
	ENGINE_SHARED virtual void update();
	ENGINE_SHARED virtual void draw();
	ENGINE_SHARED void add(Component * component);
	ENGINE_SHARED const glm::vec3 & getPosition()const;
	ENGINE_SHARED const glm::vec3 & getRotation()const;
	ENGINE_SHARED const glm::vec3 & getScale()const;
	ENGINE_SHARED void setPosition(const glm::vec3 & position);
	ENGINE_SHARED void setScale(const glm::vec3 & scale);
	ENGINE_SHARED void setRotation(const glm::vec3 & rotation);
	ENGINE_SHARED TransformComponent * getTransform()const ;
	ENGINE_SHARED void cleanUpComponents();
protected:
	std::vector<Component*> components;
	TransformComponent * transformComponent;
};

