﻿#include "RenderAppGLWindow.h"
#include <GLContext.h>
#include <Components/RenderComponent.h>
#include <Components/TransformComponent.h>
#include <QtCore/qfileinfo.h>
using glm::mat4;
RenderAppGLWindow::RenderAppGLWindow() : camera(aspectRatio()),projection(), view(), 
	backgroundColor(0.7,.3,.5)
{
	setFixedSize(QSize(700,700));
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);
}

RenderAppGLWindow::~RenderAppGLWindow()
{
	for(auto gameObject : gameObjects)
	{
		delete gameObject;
	}
}

void RenderAppGLWindow::initialize()
{
	setColor(backgroundColor);
}

void RenderAppGLWindow::update()
{
	GAME_TIMER.stop();
	view = camera.getViewMatrix();
	camera.update();
	for(auto gameObject : gameObjects)
	{
		gameObject->update();
	}

	repaint();
}

void RenderAppGLWindow::draw()
{
	makeCurrent();
	GAME_TIMER.restart();
	GL->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for(auto gameObject : gameObjects)
	{
		gameObject->draw();
	}
	doneCurrent();
}

void RenderAppGLWindow::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio(aspectRatio());
	projection = camera.getProjectionMatrix();
}

void RenderAppGLWindow::addModel(std::string fileName)
{
	//auto newGameObject = new GameObject();
	//auto newRenderComponent = new RenderComponent(newGameObject,fileName);
	//newGameObject->add(newRenderComponent);
	//newGameObject->setPosition(glm::vec3(2,0,-13));
	////QFileInfo checkFile()
	////auto fileExists
	//newRenderComponent->addShaderFile("Assets/VertexShaders/basic.vert",GL_VERTEX_SHADER);
	//newRenderComponent->addShaderFile("Assets/FragmentShaders/basic.frag",GL_FRAGMENT_SHADER);
	//newRenderComponent->buildProgram();
	//newRenderComponent->addUniform("model",MAT4,&newGameObject->getTransform()->getTransform()[0][0]);
	//newRenderComponent->addUniform("view",MAT4,&view[0][0]);
	//newRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	//gameObjects.push_back(newGameObject);

	//auto newGameObject = new GameObject();
	//auto newRenderComponent = new RenderComponent(newGameObject,"../Models/monkey.obj");
	//newGameObject->add(newRenderComponent);
	//newGameObject->setPosition(glm::vec3(2,0,-13));
	////QFileInfo checkFile()
	////auto fileExists
	//newRenderComponent->addShaderFile("Assets/VertexShaders/basic.vert",GL_VERTEX_SHADER);
	//newRenderComponent->addShaderFile("Assets/FragmentShaders/basic.frag",GL_FRAGMENT_SHADER);
	//newRenderComponent->buildProgram();
	//newRenderComponent->addUniform("model",MAT4,&newGameObject->getTransform()->getTransform()[0][0]);
	//newRenderComponent->addUniform("view",MAT4,&view[0][0]);
	//newRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	//gameObjects.push_back(newGameObject);
	makeCurrent();
	auto newGameObject = new GameObject();
	auto newRenderComponent = new RenderComponent(newGameObject,fileName.c_str());
	newGameObject->add(newRenderComponent);
	newGameObject->setPosition(glm::vec3(2,0,-13));

	newRenderComponent->addShaderFile("Assets/VertexShaders/basic.vert",GL_VERTEX_SHADER);
	newRenderComponent->addShaderFile("Assets/FragmentShaders/basic.frag",GL_FRAGMENT_SHADER);
	newRenderComponent->buildProgram();
	newRenderComponent->addUniform("model",MAT4,&newGameObject->getTransform()->getTransform()[0][0]);
	newRenderComponent->addUniform("view",MAT4,&view[0][0]);
	newRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	gameObjects.push_back(newGameObject);
	doneCurrent();
}

void RenderAppGLWindow::setColor(const glm::vec3 & newColor)
{
	backgroundColor = newColor;
	GL->glClearColor(backgroundColor.x,backgroundColor.y,backgroundColor.z,1.0f);
}

void RenderAppGLWindow::setColor(float x, float y, float z)
{
	setColor(glm::vec3(x,y,z));
}
