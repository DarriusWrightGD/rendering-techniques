#include "PlaygroundWindow.h"
#include <GLContext.h>
#include <QtCore/qdebug.h>
#include <Components/TransformComponent.h>

PlaygroundWindow::PlaygroundWindow(void) : camera(aspectRatio())
{
	setFixedSize(QSize(900,750));

}


PlaygroundWindow::~PlaygroundWindow(void)
{
	delete monkeyGameObject;
	delete monkeyRenderComponent;
}

void PlaygroundWindow::initialize()
{
	GL->glClearColor(0.0f,0.0f,0.0f,1.0f);
	monkeyGameObject = new GameObject();
	monkeyGameObject->setPosition(glm::vec3(2,0,-13));

	monkeyRenderComponent = new RenderComponent(monkeyGameObject,"../Models/monkey.obj");
	monkeyGameObject->add(monkeyRenderComponent);
	monkeyRenderComponent->addShaderFile("VertexShaders/light.vert", GL_VERTEX_SHADER);
	monkeyRenderComponent->addShaderFile("FragmentShaders/light.frag", GL_FRAGMENT_SHADER);
	monkeyRenderComponent->buildProgram();

	monkeyRenderComponent->addUniform("model", MAT4, &monkeyGameObject->getTransform()->getTransform()[0][0]);
	monkeyRenderComponent->addUniform("view", MAT4, &view[0][0]);
	monkeyRenderComponent->addUniform("projection", MAT4, &projection[0][0]);
}

void PlaygroundWindow::update()
{
	GAME_TIMER.stop();
	qDebug() << GAME_TIMER.delta();
	view = camera.getViewMatrix();
	camera.update();

	monkeyGameObject->update();
	repaint();
}

void PlaygroundWindow::draw()
{	
	GAME_TIMER.restart();
	GL->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	monkeyGameObject->draw();
	//GL->glClearColor(0.7f,0.4f,0.6f,1.0f );
}

void PlaygroundWindow::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio(aspectRatio());
	projection = camera.getProjectionMatrix();
}

///
/*
Flow of an openGL program
preform setup for the window
initialize the functions (Qt, Glew, or whatever)

/// extra funcitonality
clear the color 
enable depth buffer

setup update loop
clear depth and color buffer


///Shaders
created the shader by its type
specify the shader's content
compile the shader
(optional check for/log errors)

//Programs
once you have descided all of the programs that you are going to use
then 



*/
///
