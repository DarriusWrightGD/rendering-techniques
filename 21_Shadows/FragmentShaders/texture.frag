#version 430
uniform sampler2D basicTexture;
uniform sampler2D depthMap;
in vec2 textureCoord;
in vec3 normal;
in vec3 position;
in vec4 shadowTex;

uniform vec3 ambient;
float epsilon = 0.0042;

struct Light
{
	vec3 color;
	vec4 position;
};

struct Material
{
	vec4 diffuse;
	vec4 specular;
};
uniform Light light;
uniform Material material;

out vec4 color;

float shadowValue(vec2 texCoords)
{
	float shadow = texture(depthMap, texCoords).r;
	if(shadow + epsilon < shadowTex.z)
	{
		return 0.1;
	}
	return 1.0;
}

void main()
{
	float shadowFactor = shadowValue(shadowTex.xy);
	color = vec4(texture(basicTexture,textureCoord).xyz* shadowFactor, 1.0);
}
