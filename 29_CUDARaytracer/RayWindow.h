#pragma once

#include <QtWidgets\qwidget.h>
#include <QtWidgets\qlayout.h>
#include <QtWidgets\qlabel.h>
#include <QtCore\qtimer.h>
#include <QtCore\qelapsedtimer.h>
#include <Window\OpenGLWindow.h>
#include "cuda_runtime.h"
#include <Primitives\Sphere.h>

extern void drawRays(float time, GLuint buffer);
extern void freeCudaMem();
extern void initializeRayTracer(GLuint buffer);
class RayWindow : public OpenGLWindow
{
public:
	RayWindow(int width,int height);
	~RayWindow(void);
	 void initialize()override;
	void update()override;
	void resize(int width,int height)override;
	void draw()override;
private:
	void trace();
	unsigned char * image;
	float time;
	int windowHeight;
	int windowWidth;
	int numberOfSpheres;
	Sphere  * spheres;
	float dt;
	QElapsedTimer elapseTimer;
	GLuint bufferObj;
	const char * title;

};

