#include "HardShadowsDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>

#include <stdlib.h>
#include <GLContext.h>

float quadPos [] = 
{
	0.7f,0.7f,0.0f,
	1.0f,0.7f,0.0f,
	1.0f,1.0f,0.0f,
	1.0f,1.0f,0.0f,
	0.7f,1.0f,0.0f,
	0.7f,0.7f,0.0f,
};

float quadTex [] = 
{
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, 1.0f,
	1.0f, 1.0f,
	0.0f, 1.0f,
	0.0f, 0.0f,
};

float colorQuadPos [] = 
{
	0.4,0.4,0.0,
	0.7,0.4,0.0,
	0.7,0.7,0.0,
	0.7,0.7,0.0,
	0.4,0.7,0.0,
	0.4,0.4,0.0,
};

float colorQuadTex [] = 
{
	0.0, 0.0,
	1.0, 0.0,
	1.0, 1.0,
	1.0, 1.0,
	0.0, 1.0,
	0.0, 0.0,
};

HardShadowsDemo::HardShadowsDemo(void): lightPosition(glm::vec4(glm::vec3(2,10.0,-13),1.0f)),
	lightColor(0.4f,0.4f,0.9f),diffuse(0.2f,0.2f,0.9f,1.0f), specular(0.9f,0.0f,0.0f, 1.0f), ambientColor(0.3f,0.1,0.2), camera((float)width()/height())
{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);

}


HardShadowsDemo::~HardShadowsDemo(void)
{
	delete cubeRenderComponent;
	delete monkeyRenderComponent;
	delete floorRenderComponent;
	delete depthRenderComponent;
	delete floorObject;
	delete cubeObject;
	delete monkeyObject;
}

void HardShadowsDemo::keyPressEvent(QKeyEvent * e)
{
	if((e->key() == Qt::Key_F10))
	{
		unsigned char * buffer = new unsigned char[width() * height() * 3];
		GL->glReadPixels(0,0,width(), height(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
		QImage image(buffer,width(),height(),QImage::Format::Format_RGB888);
		image = image.mirrored();
		image.save("glSaved.jpg");
		delete [] buffer;

	}
}

void HardShadowsDemo::initialize()
{
	cubeObject = new GameObject();
	cubeObject->getTransform()->setPosition(glm::vec3(0,0,-10));
	cubeRenderComponent = new RenderComponent(cubeObject,"../Models/cube.obj");
	cubeObject->add(cubeRenderComponent);

	monkeyObject = new GameObject();
	monkeyObject->setPosition(glm::vec3(2,-1.0,-13));
	monkeyRenderComponent = new RenderComponent(monkeyObject, "../Models/monkey.obj");

	floorObject = new GameObject();
	floorObject->setPosition(glm::vec3(2,-2,-13));
	//floorObject->setScale(glm::vec3(20,20,20));
	floorRenderComponent = new RenderComponent(monkeyObject, "../Models/plane.obj");
	floorObject->add(floorRenderComponent);

	monkeyObject->add(monkeyRenderComponent);

	cubeRenderComponent->addShaderFile("VertexShaders/texture.vert", GL_VERTEX_SHADER);
	cubeRenderComponent->addShaderFile("FragmentShaders/texture.frag", GL_FRAGMENT_SHADER);
	cubeRenderComponent->buildProgram();



	monkeyRenderComponent->addShaderFile("VertexShaders/shadow.vert", GL_VERTEX_SHADER);
	monkeyRenderComponent->addShaderFile("FragmentShaders/shadow.frag", GL_FRAGMENT_SHADER);
	monkeyRenderComponent->buildProgram();

	monkeyRenderComponent->addUniform("model", MAT4, &monkeyObject->getTransform()->getTransform()[0][0]);
	monkeyRenderComponent->addUniform("shadowMatrix", MAT4, &shadowMatrix[0][0]);

	monkeyRenderComponent->addUniform("normalMatrix",MAT3,&normalMatrix[0][0]);
	monkeyRenderComponent->addUniform("light.color",VEC3,&lightColor[0]);
	monkeyRenderComponent->addUniform("material.diffuse",VEC4,&diffuse[0]);
	monkeyRenderComponent->addUniform("material.specular",VEC4,&specular[0]);
	monkeyRenderComponent->addUniform("ambient",VEC3,&ambientColor[0]);
	monkeyRenderComponent->addUniform("light.position",VEC4,&lightPosition[0]);

	floorRenderComponent->addShaderFile("VertexShaders/shadow.vert", GL_VERTEX_SHADER);
	floorRenderComponent->addShaderFile("FragmentShaders/shadow.frag", GL_FRAGMENT_SHADER);
	floorRenderComponent->buildProgram();

	floorRenderComponent->addUniform("model", MAT4, &floorObject->getTransform()->getTransform()[0][0]);
	floorRenderComponent->addUniform("normalMatrix",MAT3,&normalMatrix[0][0]);
	floorRenderComponent->addUniform("light.color",VEC3,&lightColor[0]);
	floorRenderComponent->addUniform("material.diffuse",VEC4,&diffuse[0]);
	floorRenderComponent->addUniform("material.specular",VEC4,&specular[0]);
	floorRenderComponent->addUniform("ambient",VEC3,&ambientColor[0]);
	floorRenderComponent->addUniform("light.position",VEC4,&lightPosition[0]);
	floorRenderComponent->addUniform("shadowMatrix", MAT4, &shadowMatrix[0][0]);


	cubeRenderComponent->addUniform("model", MAT4, &cubeObject->getTransform()->getTransform()[0][0]);
	cubeRenderComponent->addTexture("../Images/brick.png");
	cubeRenderComponent->addUniform("shadowMatrix", MAT4, &shadowMatrix[0][0]);




	depthRenderComponent = new RenderComponent(monkeyObject,(sizeof(quadPos)/sizeof(float))/2);
	monkeyObject->add(depthRenderComponent);
	GLuint vboV;
	GLuint vboT;
	GL->glGenBuffers(1,&vboV);
	GL->glBindBuffer(GL_ARRAY_BUFFER, vboV);
	GL->glBufferData(GL_ARRAY_BUFFER,sizeof(quadPos), quadPos,GL_STATIC_DRAW);
	GL->glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
	GL->glEnableVertexAttribArray(0);

	GL->glGenBuffers(1,&vboT);
	GL->glBindBuffer(GL_ARRAY_BUFFER, vboT);
	GL->glBufferData(GL_ARRAY_BUFFER,sizeof(quadTex), quadTex,GL_STATIC_DRAW);
	GL->glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,0,NULL);
	GL->glEnableVertexAttribArray(1);


	depthRenderComponent->addShaderFile("VertexShaders/depth.vert",GL_VERTEX_SHADER);
	depthRenderComponent->addShaderFile("FragmentShaders/depth.frag",GL_FRAGMENT_SHADER);
	depthRenderComponent->buildProgram();

	

	GL->glGenFramebuffers(1,&frameBuffer);
	GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffer);

	GL->glGenTextures(1,&frameBufferTexture);
	GL->glActiveTexture(GL_TEXTURE0);
	GL->glBindTexture(GL_TEXTURE_2D, frameBufferTexture);

	GL->glTexImage2D(GL_TEXTURE_2D,0,GL_DEPTH_COMPONENT,256,256,0,GL_DEPTH_COMPONENT,GL_UNSIGNED_BYTE,NULL);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

	GL->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, frameBufferTexture,0);

	GLenum drawBuffers[] = {GL_NONE};
	GL->glDrawBuffers(1,drawBuffers);
	if(GL->glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		qDebug() <<"Broken";

	GL->glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebufferObject());

	depthRenderComponent->addTexture(frameBufferTexture);
	monkeyRenderComponent->addTexture(frameBufferTexture);
	cubeRenderComponent->addTexture(frameBufferTexture);
	floorRenderComponent->addTexture(frameBufferTexture);






	GL->glGenBuffers(1, &cameraBuffer);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,cameraBuffer);
	GL->glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * 2, NULL, GL_DYNAMIC_DRAW);

	int blockId = 0;
	monkeyUniformBlockIndex = GL->glGetUniformBlockIndex(monkeyRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(monkeyRenderComponent->getProgramId(), monkeyUniformBlockIndex,blockId);    
	boxUniformBlockIndex = GL->glGetUniformBlockIndex(cubeRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(cubeRenderComponent->getProgramId(), boxUniformBlockIndex,blockId);   
	boxUniformBlockIndex = GL->glGetUniformBlockIndex(floorRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(floorRenderComponent->getProgramId(), boxUniformBlockIndex,blockId);   

	GL->glBindBufferBase(GL_UNIFORM_BUFFER, blockId, cameraBuffer);
	float * cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&view[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);


}

void HardShadowsDemo::update()
{

	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();

	//GL->glUniform1i(textureLocation,0);

	const float lightSpeed = 2.0f;

	if(GetAsyncKeyState('J'))
	{
		lightPosition.x -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('L'))
	{
		lightPosition.x += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('P'))
	{
		lightPosition.z -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('O'))
	{
		lightPosition.z += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('K'))
	{
		lightPosition.y -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('I'))
	{
		lightPosition.y += lightSpeed * GAME_TIMER.delta();
	}

	view = camera.getViewMatrix();
	camera.update();
	shadowMatrix = glm::lookAt(glm::vec3(lightPosition),glm::vec3(2,-2,-13),glm::vec3(0,0,-1));
	cubeObject->update();
	monkeyObject->update();
	floorObject->update();


	//GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffer);
	//GL->glClearColor(0,0,0,1.0);
	//GL->glClear(GL_DEPTH_BUFFER_BIT);
	//view = glm::lookAt(glm::vec3(lightPosition),glm::vec3(2,-2,-13),glm::vec3(0,0,-1));
	//repaint();
	//GL->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//view = camera.getViewMatrix();
	//depthRenderComponent->draw();
	repaint();
}
void HardShadowsDemo::draw() 
{
	GAME_TIMER.restart();

	GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffer);
	GL->glClearColor(0,0,0,1);
	GL->glViewport(0,0,256,256);
	GL->glClear(GL_DEPTH_BUFFER_BIT);


	view = glm::lookAt(glm::vec3(lightPosition),glm::vec3(2,0.0,-13),glm::vec3(0,0,-1));
	shadowMatrix = view;
	GL->glBindBufferBase(GL_UNIFORM_BUFFER, 0, cameraBuffer);
	float * cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&view[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);

	cubeObject->draw();
	monkeyObject->draw();
	floorObject->draw();


	GL->glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebufferObject());
	GL->glClearColor(0.4f,0.3f,0.2f,1.0f);
	GL->glViewport(0,0,width(),height());
	GL->glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);


	view = camera.getViewMatrix();

	GL->glBindBufferBase(GL_UNIFORM_BUFFER, 0, cameraBuffer);
	cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&view[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);

	cubeObject->draw();
	monkeyObject->draw();
	floorObject->draw();



}
void HardShadowsDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}
