#pragma once

#include <glm/gtx/transform.hpp>
#include <glm\vec3.hpp>
#include <glm\mat4x4.hpp>
#include <Export.h>

class Camera
{
public :
	
	ENGINE_SHARED Camera(float aspectRatio,float fieldOfView = DEFAULT_FIELD_OF_VIEW,  float nearPlaneDistance = DEFAULT_NEAR_PLANE_DISTANCE, float farPlaneDistance = DEFAULT_FAR_PLANE_DISTANCE);
	ENGINE_SHARED Camera();
	ENGINE_SHARED virtual ~Camera();
	ENGINE_SHARED const glm::vec3 & getPosition()const;
	ENGINE_SHARED glm::vec3 getDirection()const;
	ENGINE_SHARED const glm::vec3 & getLookAt()const;
	ENGINE_SHARED const glm::vec3 & getUp()const;
	ENGINE_SHARED const glm::vec3 & getRight()const;
	ENGINE_SHARED float getAspectRatio()const;
	ENGINE_SHARED float getFieldOfView()const;
	ENGINE_SHARED float getFarPlaneDifference()const;

	ENGINE_SHARED const glm::mat4 & getViewMatrix()const;
	ENGINE_SHARED const glm::mat4 & getProjectionMatrix()const;
	ENGINE_SHARED const glm::mat4 & getViewProjectionMatrix()const;
	 
	ENGINE_SHARED void setAspectRatio(float aspectRatio);
	ENGINE_SHARED virtual void setPosition(glm::vec3 position);
	ENGINE_SHARED virtual void initialize();
	 
	ENGINE_SHARED virtual void update();
	ENGINE_SHARED virtual void updateViewMatrix();
	ENGINE_SHARED virtual void updateProjectionMatrix();
	ENGINE_SHARED virtual void reset();
	ENGINE_SHARED void applyRotation(glm::mat3 rotationMatrix);

	ENGINE_SHARED static const float DEFAULT_FIELD_OF_VIEW;
	ENGINE_SHARED static const float DEFAULT_ASPECT_RATIO;
	ENGINE_SHARED static const float DEFAULT_NEAR_PLANE_DISTANCE;
	ENGINE_SHARED static const float DEFAULT_FAR_PLANE_DISTANCE;

protected:
	float fieldOfView;
	float aspectRatio;
	float nearPlaneDistance;
	float farPlaneDistance;

	glm::vec3 position;
	glm::vec3 lookAt;
	glm::vec3 up;
	glm::vec3 right;

	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	//stop the copy constructor madness
private:
	//Camera(const Camera & rhs);
	//Camera & operator=(const Camera & rhs);
};

//class Camera
//{
//
//public:
//	Camera(glm::vec3 position,float speed, float yaw , float rotationSpeed);
//	Camera();
//	void update();
//	glm::mat4 getViewMatrix()
//	{
//		glm::mat4 translation = glm::translate(-position);
//		glm::mat4 rotation = glm::rotate(-yaw, glm::vec3(0.0f,1.0f,0.0f));
//		return rotation * translation;
//	}
//
//	void setSpeed(float speed);
//	void setYaw(float yaw);
//	void setPosition(glm::vec3 position);
//
//private:
//	void checkKeys();
//	glm::vec3 position;
//	float speed;
//	float yaw;
//	float rotationSpeed;
//};

//#pragma once
//#include <glm/glm.hpp>
//#include <glm\gtx\transform.hpp>
//
//#pragma warning(disable : 4512)
//class Camera
//{
//
//
//
//	glm::vec2 oldMousePosition;
//	static const float CAMERA_SPEED;
//	static const float MOVEMENT_SPEED;
//	const glm::vec3 UP;
//
//public:
//	 Camera(void);
//	 ~Camera(void);
//	glm::vec3 position;
//	glm::vec3 viewDirection;
//	void mouseUpdate(const glm::vec2 & newMousePosition);
//
//	void moveForwards();
//	void moveBackwards();
//	void moveLeft();
//	void moveRight();
//	void moveUp();
//	void moveDown();
//
//	inline glm::mat4 getWorldToViewMatrix()const
//	{
//		return glm::lookAt(position,position+ viewDirection,UP);
//	}
//};
