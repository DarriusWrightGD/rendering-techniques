#version 430
in vec2 vp; 
in vec2 vt;
out vec2 textureCoord;

void main()
{
	textureCoord = vt;
	gl_Position = vec4(vp.xy,0,1.0);
}