#version 430
uniform sampler2D imageTexture;

in vec2 textureCoord;
out vec4 fragColor;

void main()
{
	float value = texture(imageTexture,textureCoord).x;
	if(value < 1.0)
	fragColor = vec4(value,0,0,1.0);
	else
	fragColor = vec4(value,1,1,1.0);
}