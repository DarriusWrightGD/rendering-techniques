#include <Windows.h>
#include "Camera.h"
#include "GameTimer.h"

Camera::Camera(void) :position(0.0f,0.0f,2.0f), yaw(0.0f), speed(1.0f) , rotationSpeed(0.1f)
{
}

Camera::Camera(glm::vec3 position,float speed, float yaw , float rotationSpeed) : position(position), speed(speed), yaw(yaw), rotationSpeed(rotationSpeed)
{

}

void Camera::update()
{
	checkKeys();
}

void Camera::checkKeys()
{
	if(GetAsyncKeyState('A'))
	{
		position.x -= speed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('D'))
	{
		position.x += speed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('W'))
	{
		position.z -= speed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('S'))
	{
		position.z += speed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState(VK_NEXT))
	{
		position.y -= speed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState(VK_PRIOR))
	{
		position.y += speed * GAME_TIMER.delta();
	}

	if(GetAsyncKeyState(VK_LEFT))
	{
		yaw -= speed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState(VK_RIGHT))
	{
		yaw += speed * GAME_TIMER.delta();
	}
}


void Camera::setSpeed(float speed)
{
	this->speed = speed;
}
void Camera::setYaw(float yaw)
{
	this->yaw = yaw; 
}
void Camera::setPosition(glm::vec3 position)
{
	this->position = position;
}

//#include "Camera.h"
//
//const float Camera::CAMERA_SPEED = 1.0f;
//const float Camera::MOVEMENT_SPEED = 0.1f;
//
//Camera::Camera(void) : viewDirection(0.0f,0.0f,-1.0f),UP(0.0f,1.0f,0.0f),position(0,0,10)
//{
//}
//
//
//Camera::~Camera(void)
//{
//}
//
//void Camera::mouseUpdate(const glm::vec2& mousePosition)
//{
//	glm::vec2 newmousePosition = glm::vec2(mousePosition.x,-mousePosition.y);
//	glm::vec2 mouseDelta = newmousePosition - oldMousePosition;
//
//	if(!(glm::length(mouseDelta) > 10.0f))
//	{
//		viewDirection = glm::mat3(glm::rotate(CAMERA_SPEED* mouseDelta.x,UP)) * viewDirection;
//		viewDirection = glm::mat3(glm::rotate(CAMERA_SPEED * mouseDelta.y,glm::cross(viewDirection,UP))) * viewDirection;
//	}
//
//	oldMousePosition = newmousePosition;
//
//}
//
//void Camera:: moveForwards()
//{
//	position += MOVEMENT_SPEED * viewDirection;
//}
//void Camera:: moveBackwards()
//{
//	position -= MOVEMENT_SPEED * viewDirection;
//}
//void Camera:: moveLeft()
//{
//	glm::vec3 strafeDirection = glm::cross(viewDirection,UP);
//	position -= MOVEMENT_SPEED * strafeDirection;
//}
//void Camera:: moveRight()
//{
//	glm::vec3 strafeDirection = glm::cross(viewDirection,UP);
//	position += MOVEMENT_SPEED * strafeDirection;
//}
//void Camera:: moveUp()
//{
//	position -= MOVEMENT_SPEED * UP;
//}
//void Camera:: moveDown()
//{
//	position += MOVEMENT_SPEED * UP;
//}
