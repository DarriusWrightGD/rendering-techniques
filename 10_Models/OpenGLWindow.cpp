#include "OpenGLWindow.h"
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <stdlib.h>


GLfloat points [] =  {0.0f,0.5f,0.0f,0.5f,-0.5f,0.0f,-0.5f,-0.5f,0.0f};
GLfloat colors []  = {1.0f, 0.0f, 0.0f, 0.0f ,1.0f, 0.0f,0.0f,0.0f,1.0f};
//vertex shader -> tessellation control -> tesselation evaluation ->geometry shader -> clipping -> rasterization -> fragment shader -> blending

OpenGLWindow::OpenGLWindow() : speed(1.0f), lastPosition(0.0f)
{
	setUpFormat();
}


OpenGLWindow::~OpenGLWindow(void) 
{

}

void OpenGLWindow::loadModel(const char * modelName)
{
	
	const char * filename = modelName;
	const aiScene * scene = aiImportFile(filename, aiProcess_Triangulate);
	if(!scene)
	{
		fprintf(stderr, "Error: reading mesh %s\n",filename );
		exit(-1);
	}
	printSceneInfo(scene);

	const aiMesh * mesh = scene->mMeshes[0];
	printf("\t%i vertices in mesh[0]\n", mesh->mNumVertices);
	numberOfVertices = mesh->mNumVertices;

	glm::vec3 * points = nullptr;
	glm::vec3 * normals = nullptr;
	glm::vec2 * texcoords = nullptr;

	if(mesh->HasPositions())
	{
		points = new glm::vec3[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mVertices[i];
			points[i] = glm::vec3(vertexPoint.x, vertexPoint.y,vertexPoint.z);
		}

		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices, points, GL_STATIC_DRAW);
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(0);

		delete [] points;
	}

	if(mesh->HasNormals())
	{
		normals = new glm::vec3[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mNormals[i];
			normals[i] = glm::vec3(vertexPoint.x, vertexPoint.y,vertexPoint.z);
		}

		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices, normals, GL_STATIC_DRAW);
		glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(1);

		delete [] normals;
	}

	if(mesh->HasTextureCoords(0))
	{
		texcoords = new glm::vec2[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mTextureCoords[0][i];
			texcoords[i] = glm::vec2(vertexPoint.x, vertexPoint.y);
		}

		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * numberOfVertices, texcoords, GL_STATIC_DRAW);
		glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,0,NULL);
		glEnableVertexAttribArray(2);

		delete [] texcoords;
	}
}

void OpenGLWindow::printSceneInfo(const aiScene * scene)
{
	printf("\t%i animations : \n", scene->mNumAnimations);
	printf("\t%i cameras : \n", scene->mNumCameras);
	printf("\t%i lights : \n", scene->mNumLights);
	printf("\t%i materials : \n", scene->mNumMaterials);
	printf("\t%i meshs : \n", scene->mNumMeshes);
	printf("\t%i textures : \n", scene ->mNumTextures);
}

void OpenGLWindow::initializeGL()
{

	initializeOpenGLFunctions();
	glClearColor(0.4f,0.3f,0.2f,1.0f);

	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao); 
	loadModel("../Models/monkey.obj");

	readShaderProgram();
	GAME_TIMER.restart();
	connect(&timer,&QTimer::timeout,this,&OpenGLWindow::glUpdate);
	timer.start();

	projection = getPerspectiveMatrix();

}

void OpenGLWindow::resizeGL(int w, int h)
{
	glViewport(0,0,w,h);
	setFixedSize(w,h);
	projection = getPerspectiveMatrix();
}

void OpenGLWindow::glUpdate()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();
	glUseProgram(shaderProgram);

	if(fabs(lastPosition) > 1.0f)
	{
		speed = -speed;
	}
	view = camera.getViewMatrix();
	camera.update();
	//lastPosition += speed * deltaTime;
	modelMatrix = glm::translate(glm::vec3(lastPosition,0.0f,0.0f));
	glUniformMatrix4fv(modelLocation,1,false, &modelMatrix[0][0]);
	glUniformMatrix4fv(viewLocation,1,false, &view[0][0]);
	glUniformMatrix4fv(projectionLocation,1,false, &projection[0][0]);
	repaint();
}

void OpenGLWindow::keyPressEvent(QKeyEvent * e)
{


}

void OpenGLWindow::keyReleaseEvent(QKeyEvent * e)
{

}

void OpenGLWindow::paintGL()
{
	GAME_TIMER.restart();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES,0,numberOfVertices);


}

glm::mat4 OpenGLWindow::getPerspectiveMatrix()
{
	printf("width : %i height : %i aspect ratio : %f\n", width(), height(),(float)width()/ (float)height());
	glm::mat4 perspect =  glm::perspective(glm::radians(60.0f),(float)width()/height(),0.1f,100.0f);
	return perspect;
}


void OpenGLWindow::setUpFormat()
{
	QSurfaceFormat format;
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setSamples(4);
	format.setVersion(4, 3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	setFormat(format); 
}

void OpenGLWindow::compileShader(GLuint & shader, const char * source)
{
	int params = -1;
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}
}

void OpenGLWindow::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}

void OpenGLWindow::readShaderProgram()
{
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

	compileShader(vertexShader,fileToString("VertexShaders/color.vert").c_str());

	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	compileShader(fragmentShader, fileToString("FragmentShaders/color.frag").c_str());

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, fragmentShader);
	glAttachShader(shaderProgram,vertexShader);

	glLinkProgram(shaderProgram);

	int params = -1;

	glGetProgramiv(shaderProgram , GL_LINK_STATUS, &params);
	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : could not link shader program GL index %u\n", shaderProgram);
		printShaderInfo(shaderProgram);
		system("pause");
		exit(-1);
	}
	modelLocation = glGetUniformLocation(shaderProgram, "model");
	viewLocation = glGetUniformLocation(shaderProgram, "view");
	projectionLocation = glGetUniformLocation(shaderProgram, "projection");
	modelMatrix = glm::translate(glm::vec3(0.5f,0.0f,0.0f));
	glUseProgram(shaderProgram);
	glUniformMatrix4fv(modelLocation,1,GL_FALSE, &modelMatrix[0][0]);
}


std::string OpenGLWindow::fileToString(const char * filename)
{
	return std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>());
}