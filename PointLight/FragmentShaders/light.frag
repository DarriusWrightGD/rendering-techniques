#version 430

in vec3 normal;
in vec3 position;

uniform vec3 ambient;

struct Light
{
	vec3 color;
	vec4 position;
	float constant;
	float linear;
	float quadratic;
};

struct Material
{
	vec4 diffuse;
	vec4 specular;
};
uniform Light light;
uniform Material material;

out vec4 color;

vec4 ads()
{
	vec3 direction = vec3(light.position) - position;
	vec3 lightDistance = length(direction);
	direction /=lightDistance;
	float attenuation = 1.0f /(light.constant + light.linear * lightDistance + light.quadratic * lightDistance);

	vec3 lightVector = normalize(vec3(-position));
	vec3 reflectVector = reflect(-lightVector, normal);
	return vec4(light.color  * (ambient + 
	vec3(material.diffuse) * max(dot(direction, normal),0.0) +
	 vec3(material.specular) * pow(max(dot(reflectVector, lightVector),0.0),material.specular.w)
	)
	,material.diffuse.w);
}

void main()
{
	color = ads();
}

/*
void main()
{
	vec3 direction = (light.position.w == 0) ? vec3(light.position) : vec3(light.position) - position;
	//computing the cosine of the directions, using the dot products
	// to seed how much light would be reflected...
	
	vec3 halfVector = normalize(direction + (-vertexPosition));
	float diffuseComponent = max(0.0, dot(normal, direction));
	float specularComponent = max(0.0, dot(normal, halfVector));

	specularComponent = (diffuseComponent == 0.0) ? 0.0 : pow(specularComponent, material.shine);
	
	//ambient + diffuse
	vec3 scatteredLight = ambient + light.color * diffuseComponent;
	
	//specular
	vec3 reflectedLight = light.color * specularComponent;

	//summing of light color
	vec3 rgb = min(material.color.rgb * scatteredLight + reflectedLight, vec3(1.0));
	color = vec4(rgb,material.color.a);
}*/