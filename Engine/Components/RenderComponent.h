#pragma once
#include <Components\Component.h>
#include <GL\Program.h>
#include <map>
#include <vector>
class GameObject;
class RenderComponent : public Component
{
	RTTI_DECLARATIONS(RenderComponent,Component);
public:
	ENGINE_SHARED RenderComponent(GameObject * gameObject, const char * filename);
	ENGINE_SHARED RenderComponent(GameObject * gameObject, int numberOfVertices);
	ENGINE_SHARED virtual ~RenderComponent(void);
	ENGINE_SHARED virtual void draw();
	ENGINE_SHARED void addUniform(const char * name,UniformType type, float * data);
	ENGINE_SHARED void addTexture(const char * imagePath);
	ENGINE_SHARED void addTexture(GLuint textureId);
	ENGINE_SHARED void addShaderFile(const std::string & filename, GLenum type);
	ENGINE_SHARED void addShaderSource(const std::string & source, GLenum type);
	ENGINE_SHARED void buildProgram();
	ENGINE_SHARED Program getProgram();
	ENGINE_SHARED GLuint getProgramId();
	ENGINE_SHARED void setProgram(const Program & program);
protected:
	void loadModel(const char * filename);
	GLuint numberOfVertices;
	GLuint vao;
	Program program;
};

