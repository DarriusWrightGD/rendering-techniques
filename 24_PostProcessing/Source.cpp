#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "EdgeDetectionDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	EdgeDetectionDemo widget;
	widget.show();
	return app.exec();
}