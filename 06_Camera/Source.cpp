#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "OpenGLWindow.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	OpenGLWindow widget;
	widget.show();
	return app.exec();
}