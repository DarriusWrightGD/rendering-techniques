#include "PhongDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <stdlib.h>
#include <assimp\Importer.hpp>
#include <GLContext.h>

PhongDemo::PhongDemo(void) :  speed(1.0f), lastPosition(0.0f), lightPosition(0,0,-2),
	lightLa(0.9,0.1,0.1),lightLd(0.1,0.3,0.9),lightLs(0.2,0.2,0.1),
	modelMs(0.4,0.1,0.5),modelMa(0.3,0.3,0.7),modelMd(0.7,0.2,0.4),camera((float)width()/height())
{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);
	
}


PhongDemo::~PhongDemo(void)
{
}

void PhongDemo::loadModel(const char * modelName)
{

	const char * filename = modelName;

	//Assimp::Importer importer;
	//const aiScene * scene = importer.ReadFile(filename, flags);


	const aiScene * scene = aiImportFile(filename,aiProcess_Triangulate);
	if(!scene)
	{
		fprintf(stderr, "Error: reading mesh %s\n",filename );
		exit(-1);
	}
	printSceneInfo(scene);

	const aiMesh * mesh = scene->mMeshes[0];
	printf("\t%i vertices in mesh[0]\n", mesh->mNumVertices);
	numberOfVertices = mesh->mNumVertices;

	glm::vec3 * points = nullptr;
	glm::vec3 * normals = nullptr;
	glm::vec2 * texcoords = nullptr;

	if(mesh->HasPositions())
	{
		points = new glm::vec3[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mVertices[i];
			points[i] = glm::vec3(vertexPoint.x, vertexPoint.y,vertexPoint.z);
		}

		GLuint vbo;
		GL->glGenBuffers(1, &vbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, vbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices, points, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
		GL->glEnableVertexAttribArray(0);

		delete [] points;
	}

	if(mesh->HasNormals())
	{
		normals = new glm::vec3[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D normalPoint = mesh->mNormals[i];
			normals[i] = glm::vec3(normalPoint.x, normalPoint.y,normalPoint.z);
		}

		GLuint nbo;
		GL->glGenBuffers(1, &nbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, nbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices , normals, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,NULL);
		
		GL->glEnableVertexAttribArray(1);

		delete [] normals;
	}

	if(mesh->HasTextureCoords(0))
	{
		texcoords = new glm::vec2[numberOfVertices];
		for (int i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mTextureCoords[0][i];
			texcoords[i] = glm::vec2(vertexPoint.x, vertexPoint.y);
		}

		GLuint vbo;
		GL->glGenBuffers(1, &vbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, vbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * numberOfVertices, texcoords, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,0,NULL);
		GL->glEnableVertexAttribArray(2);

		delete [] texcoords;
	}
}

void PhongDemo::printSceneInfo(const aiScene * scene)
{
	printf("\t%i animations : \n", scene->mNumAnimations);
	printf("\t%i cameras : \n", scene->mNumCameras);
	printf("\t%i lights : \n", scene->mNumLights);
	printf("\t%i materials : \n", scene->mNumMaterials);
	printf("\t%i meshs : \n", scene->mNumMeshes);
	printf("\t%i textures : \n", scene ->mNumTextures);
}
void PhongDemo::compileShader(GLuint & shader, const char * source)
{
	int params = -1;
	GL->glShaderSource(shader, 1, &source, NULL);
	GL->glCompileShader(shader);
	GL->glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}
}

void PhongDemo::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	GL->glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}

void PhongDemo::readShaderProgram()
{
	GLuint vertexShader = GL->glCreateShader(GL_VERTEX_SHADER);

	compileShader(vertexShader,fileToString("VertexShaders/light.vert").c_str());

	GLuint fragmentShader = GL->glCreateShader(GL_FRAGMENT_SHADER);
	compileShader(fragmentShader, fileToString("FragmentShaders/light.frag").c_str());

	shaderProgram = GL->glCreateProgram();
	GL->glAttachShader(shaderProgram, fragmentShader);
	GL->glAttachShader(shaderProgram,vertexShader);

	GL->glLinkProgram(shaderProgram);

	int params = -1;

	GL->glGetProgramiv(shaderProgram , GL_LINK_STATUS, &params);
	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : could not link shader program GL index %u\n", shaderProgram);
		printShaderInfo(shaderProgram);
		system("pause");
		exit(-1);
	}
	modelLocation = GL->glGetUniformLocation(shaderProgram, "model");
	viewLocation = GL->glGetUniformLocation(shaderProgram, "view");
	projectionLocation = GL->glGetUniformLocation(shaderProgram, "projection");
	lightLsLocation = GL->glGetUniformLocation(shaderProgram, "light.Ls");
	lightLdLocation = GL->glGetUniformLocation(shaderProgram, "light.Ld");
	lightLaLocation = GL->glGetUniformLocation(shaderProgram, "light.La");
	modelMsLocation = GL->glGetUniformLocation(shaderProgram, "material.Ks");
	modelMdLocation = GL->glGetUniformLocation(shaderProgram, "material.Kd");
	modelMaLocation = GL->glGetUniformLocation(shaderProgram, "material.Ka");
	normalLocation  = GL->glGetUniformLocation(shaderProgram, "normalMatrix");
	modelViewLocation = GL->glGetUniformLocation(shaderProgram, "modelView");
	lightPositionLocation = GL->glGetUniformLocation(shaderProgram, "light.position");


	modelMatrix = glm::translate(glm::vec3(0.5f,0.0f,0.0f));
	GL->glUseProgram(shaderProgram);
	GL->glUniformMatrix4fv(modelLocation,1,GL_FALSE, &modelMatrix[0][0]);
	GL->glUniform3fv(lightLsLocation,1,&lightLs[0]);
	GL->glUniform3fv(lightLaLocation,1,&lightLa[0]);
	GL->glUniform3fv(lightLdLocation,1,&lightLd[0]);
	GL->glUniform3fv(modelMsLocation,1,&modelMs[0]);
	GL->glUniform3fv(modelMaLocation,1,&modelMa[0]);
	GL->glUniform3fv(modelMdLocation,1,&modelMd[0]);
	GAME_TIMER.restart();
}

std::string PhongDemo::fileToString(const char * filename)
{
	return std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>());
}

void PhongDemo::initialize()
{
	GL->glGenVertexArrays(1,&vao);
	GL->glBindVertexArray(vao); 
	loadModel("../Models/hiMonkey.obj");
	readShaderProgram();
}
void PhongDemo::update()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();
	GL->glUseProgram(shaderProgram);

	const float lightSpeed = 2.0f;

	if(keyboard.keyPressed('J'))
	{
		lightPosition.x -= lightSpeed * GAME_TIMER.delta();
	}
	if(keyboard.keyPressed('L'))
	{
		lightPosition.x += lightSpeed * GAME_TIMER.delta();
	}
	if(keyboard.keyPressed('P'))
	{
		lightPosition.z -= lightSpeed * GAME_TIMER.delta();
	}
	if(keyboard.keyPressed('O'))
	{
		lightPosition.z += lightSpeed * GAME_TIMER.delta();
	}
	if(keyboard.keyPressed('K'))
	{
		lightPosition.y -= lightSpeed * GAME_TIMER.delta();
	}
	if(keyboard.keyPressed('I'))
	{
		lightPosition.y += lightSpeed * GAME_TIMER.delta();
	}

	view = camera.getViewMatrix();
	camera.update();

	modelMatrix = glm::translate(glm::vec3(lastPosition,0.0f,-5.0f));
	normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelMatrix)));
	modelView = view *  modelMatrix;

	GL->glUniformMatrix4fv(modelLocation,1,false, &modelMatrix[0][0]);
	GL->glUniformMatrix3fv(normalLocation,1,false, &normalMatrix[0][0]);
	GL->glUniformMatrix4fv(modelViewLocation,1,false, &modelView[0][0]);
	GL->
	GL->glUniformMatrix4fv(viewLocation,1,false, &view[0][0]);
	GL->glUniformMatrix4fv(projectionLocation,1,false, &projection[0][0]);
	GL->glUniform3fv(lightPositionLocation,1,&lightPosition[0]);
	GL->glUniform3fv(lightLsLocation,1,&lightLs[0]);
	GL->glUniform3fv(lightLaLocation,1,&lightLa[0]);
	GL->glUniform3fv(lightLdLocation,1,&lightLd[0]);
	GL->glUniform3fv(modelMsLocation,1,&modelMs[0]);
	GL->glUniform3fv(modelMaLocation,1,&modelMa[0]);
	GL->glUniform3fv(modelMdLocation,1,&modelMd[0]);
}
void PhongDemo::draw() 
{
	GAME_TIMER.restart();
	GL->glBindVertexArray(vao);
	GL->glDrawArrays(GL_TRIANGLES,0,numberOfVertices);
}
void PhongDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}