#include "FragmentRayTracingDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>

#include <stdlib.h>
#include <GLContext.h>



FragmentRayTracingDemo::FragmentRayTracingDemo(void): camera((float)width()/height())

{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,-100));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);

}


FragmentRayTracingDemo::~FragmentRayTracingDemo(void)
{

}

void FragmentRayTracingDemo::keyPressEvent(QKeyEvent * e)
{
	if((e->key() == Qt::Key_F10))
	{
		auto buffer = new unsigned char[width() * height() * 3];
		GL->glReadPixels(0,0,width(), height(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
		QImage image(buffer,width(),height(),QImage::Format::Format_RGB888);
		image = image.mirrored();
		image.save("glSaved.jpg");
		delete [] buffer;
	}

}


void FragmentRayTracingDemo::initialize()
{
	prepareProgram.addShaderFile("VertexShader/prepare.vert",GL_VERTEX_SHADER);
	prepareProgram.addShaderFile("FragmentShader/prepare.frag",GL_FRAGMENT_SHADER);
	prepareProgram.buildProgram();

	traceProgram.addShaderFile("VertexShader/trace.vert",GL_VERTEX_SHADER);
	traceProgram.addShaderFile("FragmentShader/trace.frag",GL_FRAGMENT_SHADER);
	traceProgram.buildProgram();

	drawProgram.addShaderFile("VertexShader/draw.vert",GL_VERTEX_SHADER);
	drawProgram.addShaderFile("FragmentShader/draw.frag",GL_FRAGMENT_SHADER);
	drawProgram.buildProgram();



	GL->glGenBuffers(1,&uniformsUBO);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,uniformsUBO);
	GL->glBufferData(GL_UNIFORM_BUFFER,sizeof(UniformBlock),NULL,GL_DYNAMIC_DRAW);
	GL->glGenBuffers(1,&sphereUBO);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,sphereUBO);
	GL->glBufferData(GL_UNIFORM_BUFFER,NUM_SPHERES*sizeof(Sphere),NULL,GL_DYNAMIC_DRAW);
	GL->glGenBuffers(1,&planeUBO);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,planeUBO);
	GL->glBufferData(GL_UNIFORM_BUFFER,NUM_PLANES*sizeof(Plane),NULL,GL_DYNAMIC_DRAW);
	GL->glGenBuffers(1,&lightUBO);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,lightUBO);
	GL->glBufferData(GL_UNIFORM_BUFFER,NUM_LIGHTS*sizeof(Light),NULL,GL_DYNAMIC_DRAW);

	GL->glGenFramebuffers(MAX_TRACE_DEPTH,frameBuffers);
	GL->glGenTextures(1,&fullImage);
	GL->glGenTextures(MAX_TRACE_DEPTH,positionTex);
	GL->glGenTextures(MAX_TRACE_DEPTH,reflectedTex);
	GL->glGenTextures(MAX_TRACE_DEPTH,refractionTex);
	GL->glGenTextures(MAX_TRACE_DEPTH,reflectionIntesityTex);
	GL->glGenTextures(MAX_TRACE_DEPTH,refractionIntensityTex);

	GL->glBindTexture(GL_TEXTURE_2D,fullImage);
	GL->glTexStorage2D(GL_TEXTURE_2D,1,GL_RGB16F,width(),height());

	for (int i = 0; i < MAX_TRACE_DEPTH; i++)
	{
		GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffers[i]);
		GL->glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,fullImage,0);

		GL->glBindTexture(GL_TEXTURE_2D,positionTex[i]);
		GL->glTexStorage2D(GL_TEXTURE_2D, 1,GL_RGBA32F,width(),height());
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		GL->glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT1,positionTex[i],0);

		GL->glBindTexture(GL_TEXTURE_2D,reflectedTex[i]);
		GL->glTexStorage2D(GL_TEXTURE_2D, 1,GL_RGBA16F,width(),height());
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		GL->glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT2,reflectedTex[i],0);

		GL->glBindTexture(GL_TEXTURE_2D,refractionTex[i]);
		GL->glTexStorage2D(GL_TEXTURE_2D, 1,GL_RGBA16F,width(),height());
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		GL->glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT3,refractionTex[i],0);

		GL->glBindTexture(GL_TEXTURE_2D,reflectionIntesityTex[i]);
		GL->glTexStorage2D(GL_TEXTURE_2D, 1,GL_RGBA16F,width(),height());
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		GL->glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT4,reflectionIntesityTex[i],0);

		GL->glBindTexture(GL_TEXTURE_2D,refractionIntensityTex[i]);
		GL->glTexStorage2D(GL_TEXTURE_2D, 1,GL_RGBA16F,width(),height());
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		GL->glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT5,refractionIntensityTex[i],0);

	}

	GL->glBindFramebuffer(GL_FRAMEBUFFER,0);
	GL->glBindTexture(GL_TEXTURE_2D, 0);
}

void FragmentRayTracingDemo::update()
{
	GAME_TIMER.stop();
	deltaTime = GAME_TIMER.delta();
	totalTime += deltaTime;
	camera.update();
	//view = camera.getViewMatrix();
	repaint();
}
void FragmentRayTracingDemo::draw() 
{
	GAME_TIMER.restart();
	glm::vec3 viewPosition = glm::vec3(sinf(totalTime * 0.3234f) * 28.0f, cosf(totalTime * 0.4234f) * 28.0f, cosf(totalTime * 0.1234f) * 28.0f); // sinf(f * 0.2341f) * 20.0f - 8.0f);
	glm::vec3 lookAtPoint = glm::vec3(sinf(totalTime * 0.214f) * 8.0f, cosf(totalTime * 0.153f) * 8.0f, sinf(totalTime * 0.734f) * 8.0f);
	glm::mat4 view = glm::lookAt(viewPosition, lookAtPoint, glm::vec3(0.0f, 1.0f, 0.0f));
	
	GL->glBindBufferBase(GL_UNIFORM_BUFFER,0,uniformsUBO);
	UniformBlock * block = (UniformBlock *)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0,sizeof(UniformBlock),
		GL_MAP_WRITE_BIT|GL_MAP_INVALIDATE_BUFFER_BIT);
	
	glm::mat4 model = glm::scale(glm::vec3(7.0f,7.0f,7.0f));
	block->modelViewMatrix = view * model;
	block->viewMatrix = view;
	block->projectionMatrix = projection;

	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);

	GL->glBindBufferBase(GL_UNIFORM_BUFFER,0,sphereUBO);
	Sphere * spheres = (Sphere *)GL->glMapBufferRange(GL_UNIFORM_BUFFER, 0, NUM_SPHERES * sizeof(Sphere),
		GL_MAP_WRITE_BIT|GL_MAP_INVALIDATE_BUFFER_BIT);
}


void FragmentRayTracingDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}
