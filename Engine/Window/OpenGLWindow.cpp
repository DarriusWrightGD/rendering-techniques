#include "OpenGLWindow.h"
#include <iostream>
#include <fstream>
#include <ostream>
#include <GLContext.h>

OpenGLWindow::OpenGLWindow()
{
	setMouseTracking(true);
	setUpFormat();

}

OpenGLWindow::~OpenGLWindow(void) 
{

}

void OpenGLWindow::mouseMoveEvent(QMouseEvent * e)
{
	mouse.setPosition(glm::vec2(e->x(), e->y()));
}

void OpenGLWindow::mousePressEvent(QMouseEvent * e)
{
	mouse.setLeftButtonDown((e->buttons() == Qt::LeftButton));
	mouse.setRightButtonDown((e->buttons() == Qt::RightButton));
	mouse.setPosition(glm::vec2(e->x(), e->y()));
}

void OpenGLWindow::mouseReleaseEvent(QMouseEvent * e)
{
	if(!(e->buttons() & Qt::LeftButton))
		mouse.setLeftButtonDown(false);
	if(!(e->buttons() & Qt::RightButton))
		mouse.setRightButtonDown(false);

	mouse.setPosition(glm::vec2(e->x(), e->y()));
}

void OpenGLWindow::initializeGL()
{
	GL->initializeOpenGLFunctions();
	GL->glClearColor(0.4f,0.3f,0.2f,1.0f);
	GL->glEnable(GL_DEPTH_TEST);
	initialize();
	GAME_TIMER.restart();
	connect(&timer,&QTimer::timeout,this,&OpenGLWindow::glUpdate);
	timer.start();
}

void OpenGLWindow::resizeGL(int w, int h)
{
	resize(w,h);
}

void OpenGLWindow::glUpdate()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();
	update();
	repaint();
	mouse.setPosition(mouse.getPosition());
}


void OpenGLWindow::paintGL()
{
	GAME_TIMER.restart();
	GL->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	draw();
}

void OpenGLWindow::setUpFormat()
{
	QSurfaceFormat format;
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setSamples(4);
	format.setVersion(4, 5);
	format.setProfile(QSurfaceFormat::CompatibilityProfile);
	create();
	setFormat(format); 
}

float OpenGLWindow::aspectRatio() const
{
	return static_cast<float>(width())/ static_cast<float>(height());
}