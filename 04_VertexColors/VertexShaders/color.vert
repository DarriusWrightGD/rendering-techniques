#version 430

// doesn't work on mac
//layout (location = 0) in vec3 vertexPoint;
//layout (location = 1)in vec3 vertexColor;

in vec3 vertexPoint;
in vec3 vertexColor;
out vec3 color;

void main () 
{
	color = vertexColor;
	gl_Position = vec4(vertexPoint, 1.0);
}