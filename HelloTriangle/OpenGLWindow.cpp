#include "OpenGLWindow.h"
#include <iostream>
#include <fstream>
#include <ostream>

GLfloat points [] =  {0.0f,0.5f,0.0f,0.5f,-0.5f,0.0f,-0.5f,-0.5f,0.0f};





//vertex shader -> tessellation control -> tesselation evaluation ->geometry shader -> clipping -> rasterization -> fragment shader -> blending


const char * vertex_shader = 
	"#version 430\n"
	"in vec3 vertexPoint;"
	"void main () {"
	" gl_Position = vec4(vertexPoint, 1.0);}";

const char * fragment_shader = 
	"#version 430\n"
	"out vec4 frag_color;"
	"void main(){frag_color = vec4(0.5,0.0,0.5,1.0);}";

OpenGLWindow::OpenGLWindow() 
{
	setUpFormat();
}


OpenGLWindow::~OpenGLWindow(void) 
{

}


void OpenGLWindow::initializeGL()
{

	initializeOpenGLFunctions();
	glClearColor(0.4f,0.3f,0.2f,1.0f);
	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);

	glGenBuffers(1,&vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 0, NULL);

	readShaderProgram();

}

void OpenGLWindow::resizeGL(int w, int h)
{

}

void OpenGLWindow::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shaderProgram);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES,0,3);


}

void OpenGLWindow::setUpFormat()
{
	QSurfaceFormat format;
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setSamples(4);
	format.setVersion(4, 3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	setFormat(format); 
}

void OpenGLWindow::compileShader(GLuint & shader, const char * source)
{
	int params = -1;
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &params);
	
	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}
}

void OpenGLWindow::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}

void OpenGLWindow::readShaderProgram()
{
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	
	compileShader(vertexShader,fileToString("VertexShaders/color.vert").c_str());
	
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	compileShader(fragmentShader, fileToString("FragmentShaders/color.frag").c_str());
	
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, fragmentShader);
	glAttachShader(shaderProgram,vertexShader);

	glLinkProgram(shaderProgram);

	int params = -1;

	glGetProgramiv(shaderProgram , GL_LINK_STATUS, &params);
	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : could not link shader program GL index %u\n", shaderProgram);
		printShaderInfo(shaderProgram);
		system("pause");
		exit(-1);
	}

}

std::string OpenGLWindow::fileToString(const char * filename)
{
	return std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>());
}