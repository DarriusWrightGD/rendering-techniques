#pragma once
#include <Window\OpenGLWindow.h>
#include <assimp\scene.h>
#include <Camera\FirstPersonCamera.h>
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglcolormap.h>
#include <QtGui\qopengltexture.h>
#include <map>
#include <GameObjects\GameObject.h>
#include <Components\GuiComponent.h>
#include <Components\RenderComponent.h>
#include <Components\TransformComponent.h>
// Start the concept of the model, material, and uniforms  
/*
the model knows the number of vertices that it has, also its arraybuffer 
the material will know its uniforms and the information that it points to float *
the uniform will hold the index that the uniform is located at
*/

class TextureSlicingDemo : public OpenGLWindow
{
public:
	TextureSlicingDemo(void);
	~TextureSlicingDemo(void);

	virtual void initialize()override;
	virtual void update()override;
	virtual void draw() override;
	virtual void resize(int width, int height)override;
	virtual void keyPressEvent(QKeyEvent * e)override;
private:
	
	GameObject * cubeObject;
	GameObject * screenObject;
	RenderComponent * cubeRenderComponent;

	GameObject * monkeyObject;
	RenderComponent * monkeyRenderComponent;
	GLuint cameraBuffer;

	//RenderComponent * colorRenderComponent;
	//RenderComponent * depthRenderComponent;
	GuiComponent * colorGuiComponent;
	//GuiComponent * depthGuiComponent;
	static const int MAX_SLICES = 512;
	glm::mat4 model;
	int findAbsMax(glm::vec3 vector);
	void calculateDistance();
	float minDistance;
	float maxDistance;
	int numberOfSlices;
	glm::vec3 textureSlices[MAX_SLICES * 12];
	GLuint mvpLocation;
	glm::mat4 mvp;
	GLuint program;
	std::vector<GLuint> shaders;
	void addShaderFile(const std::string & filename, GLenum type);
	void addShaderSource(const std::string & source, GLenum type);
	void buildProgram();
	void printShaderInfo(GLuint shader);

	GLuint volumeTexture;
	GLuint volumeVao;
	GLuint volumeVbo;

	GLuint monkeyUniformBlockIndex;
	GLuint boxUniformBlockIndex;
	FirstPersonCamera camera;

	glm::vec3 lightColor;
	glm::vec3 ambientColor;
	glm::vec4 lightPosition;

	QTimer timer;

	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat3 normalMatrix;
	glm::mat4 modelView;

	GLint numberOfBoxVertices;
	GLint numberOfMonkeyVertices;
	GLuint colorBuffer;
	GLuint depthBuffer;
	GLuint frameBuffer;
	GLuint secondaryFrameBuffer;
	GLuint postProcessLocation;
	GLuint edgeDetectionIndex;
};