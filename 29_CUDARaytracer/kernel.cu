#include <GLContext.h>
#include "RayWindow.h"
#include "cuda_runtime.h"
#include "cuda_gl_interop.h"
#include "device_launch_parameters.h"
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qlabel.h>
#include <QtWidgets\qapplication.h>
#include <stdio.h>
//#define GLM_FORCE_CUDA
#include <glm\glm.hpp>
#include <glm\common.hpp>
#include "math_functions.h"
#include "math_constants.h"
#include "CPURayWindow.h"
#include <Primitives\Sphere.h>
#include <glm\gtc\random.hpp>
#include <Camera\RayCamera.h>
#include <random>
#include <Primitives\SceneInfo.h>



int imageSize;// = width * height * channels;
Sphere * spheres;
cudaGraphicsResource * resource;
SceneInfo sceneInfo;
unsigned char * image;





__global__ void colorImage(unsigned char * image,RayCamera camera, Sphere * spheres, SceneInfo scene)
{
	int y = blockIdx.y * blockDim.y  + threadIdx.y;
	int x =  blockIdx.x * blockDim.x + threadIdx.x;
	if(x <= scene.width && y <= scene.height)
	{
		glm::vec3 pixelPoint((float)(x-(scene.width)/2),(float)(y -(scene.height)/2),100);
		glm::vec3 direction(0,0,-1);
		//Ray ray;
		//ray.origin = pixelPoint;
		//ray.direction(0,0,-1);
		float max = -infinity_f();
		float t ;
		float r = 0, g = 0, b = 0;
		glm::vec3 normal;
		for(int i =0;i < scene.numberOfSpheres;i++ )
		{
			//t = spheres[i].hit(pixelPoint.x,pixelPoint.y);
			t = spheres[i].hit(pixelPoint,direction, &normal);
			
			if(t  > max)
			{
				r= spheres[i].color.r;
				g= spheres[i].color.g;
				b= spheres[i].color.b;
				max = t;
			}
		}

		int pixelStart = (x + y * blockDim.x * gridDim.x) * 4;
		if(pixelStart < scene.width* scene.height *4)
		{
			image[pixelStart+0] = (int)(r*255);
			image[pixelStart+1] = (int)(g*255);
			image[pixelStart+2] = (int)(b*255);
			image[pixelStart+3] = 255;
		}
	}

}

void  drawRays( float time,GLuint buffer)
{
	size_t size;
	cudaGraphicsMapResources(buffer,&resource,NULL);
	cudaGraphicsResourceGetMappedPointer((void**)&image,&size,resource);

	dim3 threads(16,16);
	dim3 grids(sceneInfo.width/threads.x,sceneInfo.height/threads.y);
	RayCamera camera(glm::vec3(0,0,-5), glm::vec3(0,0,0));
	colorImage <<<grids,threads>>> (image,camera, spheres,sceneInfo);
	cudaGraphicsUnmapResources(1,&resource,NULL);


}

void initializeSceneInfo(int numberOfSpheres = 10, int sampleWidth = 1, int sampleHeight = 1, int width = 512, int height = 384, int channels  =4)
{
	sceneInfo.width = width;
	sceneInfo.height = height;
	sceneInfo.numberOfSpheres = numberOfSpheres;
	sceneInfo.setSampleWidth(sampleWidth);
	sceneInfo.setSampleHeight(sampleHeight );
	sceneInfo. channels = channels;
}
void initializeRayTracer(GLuint buffer)
{

	imageSize = sceneInfo.channels * sceneInfo.width * sceneInfo.height;

	Sphere * tempSpheres = new Sphere[sceneInfo.numberOfSpheres];

	for (int i = 0; i < sceneInfo.numberOfSpheres; i++)
	{
		glm::vec3 position(random(glm::vec3(-100,-100,-100), glm::vec3(100,100,-50)));
		glm::vec3 color(random(glm::vec3(0.0,0.0,0.0), glm::vec3(1.0,1.0,1.0)));

		float size = random(20.0f,50.0f);
		Sphere sphere = {color, size, position};
		tempSpheres[i] = sphere;
	}
	cudaMalloc(&spheres,sizeof(Sphere) * sceneInfo.numberOfSpheres);
	cudaMemcpy(spheres, tempSpheres,sceneInfo.numberOfSpheres * sizeof(Sphere),cudaMemcpyKind::cudaMemcpyHostToDevice);
	delete [] tempSpheres;

	size_t size;

	cudaGraphicsGLRegisterBuffer(&resource,buffer,cudaGraphicsMapFlags::cudaGraphicsMapFlagsNone);
}

void freeCudaMem()
{
	cudaFree(image);
	cudaFree(spheres);
	cudaGraphicsUnregisterResource(resource);
}

int main(int argc, char ** argv)
{
	initializeSceneInfo();
	QApplication app(argc,argv);
	RayWindow window(sceneInfo.width,sceneInfo.height);
	srand(time(0));
	window.show();
	return app.exec();
}
