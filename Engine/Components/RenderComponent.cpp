#include "RenderComponent.h"
#include <GLContext.h>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>
#include <glm\glm.hpp>


RTTI_DEFINITIONS(RenderComponent);

RenderComponent::RenderComponent(GameObject * gameObject, const char * filename) : Component(gameObject)
{
	GL->glGenVertexArrays(1,&vao);
	GL->glBindVertexArray(vao); 
	if(filename != nullptr)
		loadModel(filename);
}

RenderComponent::RenderComponent(GameObject * gameObject, int numberOfVertices) : Component(gameObject)
{
	GL->glGenVertexArrays(1,&vao);
	GL->glBindVertexArray(vao); 
	this->numberOfVertices = numberOfVertices;
}



RenderComponent::~RenderComponent(void)
{
}

void RenderComponent::loadModel(const char * filename)
{

	const aiScene * scene = aiImportFile(filename,aiProcess_Triangulate);
	if(!scene)
	{
		fprintf(stderr, "Error: reading mesh %s\n",filename );
		exit(-1);
	}

	const aiMesh * mesh = scene->mMeshes[0];
	numberOfVertices = mesh->mNumVertices;
	

	glm::vec3 * points = nullptr;
	glm::vec3 * normals = nullptr;
	glm::vec2 * texcoords = nullptr;

	if(mesh->HasPositions())
	{
		points = new glm::vec3[numberOfVertices];
		for (size_t i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mVertices[i];
			points[i] = glm::vec3(vertexPoint.x, vertexPoint.y,vertexPoint.z);
		}

		GLuint vbo;
		GL->glGenBuffers(1, &vbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, vbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices, points, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,NULL);
		GL->glEnableVertexAttribArray(0);

		delete [] points;
	}

	if(mesh->HasNormals())
	{
		normals = new glm::vec3[numberOfVertices];
		for (size_t i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D normalPoint = mesh->mNormals[i];
			normals[i] = glm::vec3(normalPoint.x, normalPoint.y,normalPoint.z);
		}

		GLuint nbo;
		GL->glGenBuffers(1, &nbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, nbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numberOfVertices , normals, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,NULL);

		GL->glEnableVertexAttribArray(1);

		delete [] normals;
	}

	if(mesh->HasTextureCoords(0))
	{
		texcoords = new glm::vec2[numberOfVertices];
		for (size_t i = 0; i < numberOfVertices; i++)
		{
			const aiVector3D vertexPoint = mesh->mTextureCoords[0][i];
			texcoords[i] = glm::vec2(vertexPoint.x, vertexPoint.y);
		}

		GLuint vbo;
		GL->glGenBuffers(1, &vbo);
		GL->glBindBuffer(GL_ARRAY_BUFFER, vbo);
		GL->glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * numberOfVertices, texcoords, GL_STATIC_DRAW);
		GL->glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,0,NULL);
		GL->glEnableVertexAttribArray(2);

		delete [] texcoords;
	}
}

void RenderComponent::setProgram(const Program & program)
{
	this->program = program;
}

void RenderComponent::draw()
{
	program.updateUniforms();
	GL->glBindVertexArray(vao);
	GL->glDrawArrays(GL_TRIANGLES,0,numberOfVertices);
}

Program RenderComponent::getProgram()
{
	return program;
}

void RenderComponent::addUniform(const char * name,UniformType type, float * data)
{
	program.addUniform(name, type, data);
}

void RenderComponent::addTexture(const char * imagePath)
{
	program.addTexture(imagePath);
}

void RenderComponent::addTexture(GLuint textureId)
{
	program.addTexture(textureId);
}

GLuint RenderComponent::getProgramId()
{
	return program.getProgram();
}

void RenderComponent::addShaderFile(const std::string & filename, GLenum type)
{
	program.addShaderFile(filename, type);
}
void RenderComponent::addShaderSource(const std::string & source, GLenum type)
{
	program.addShaderSource(source,type);
}

void RenderComponent::buildProgram()
{
	program.buildProgram();
}
