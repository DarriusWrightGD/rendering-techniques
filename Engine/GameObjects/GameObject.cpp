#include "GameObject.h"
#include <Components\TransformComponent.h>
#include <Components\Component.h>
#include <Components\RenderComponent.h>
RTTI_DEFINITIONS(GameObject);

GameObject::GameObject()
{
	transformComponent = new TransformComponent(this);
	
}


GameObject::~GameObject(void)
{
	delete transformComponent;
}


void GameObject::update()
{
	for(Component * component : components)
	{
		component->update();
	}
}

void GameObject::draw()
{
	for(Component * component : components)
	{
		if(component->is(RenderComponent::typeIdClass()))
		{
			RenderComponent * renderComponent = component->as<RenderComponent>();
			renderComponent->draw();
		}
	}
}


void GameObject::add(Component * component)
{
	components.push_back(component);
}
const glm::vec3 & GameObject:: getPosition()const
{
	return transformComponent->getPosition();
}
const glm::vec3 & GameObject::getRotation()const
{
	return transformComponent->getRotation();
}
const glm::vec3 & GameObject::getScale()const
{
	return transformComponent->getScale();
}
void GameObject::setPosition(const glm::vec3 & position)
{
	transformComponent->setPosition(position);
}
void GameObject::setScale(const glm::vec3 & scale)
{
	transformComponent->setScale(scale);
}
void GameObject::setRotation(const glm::vec3 & rotation)
{
	transformComponent->setRotation(rotation);
}
TransformComponent * GameObject::getTransform() const
{
	return transformComponent;
}

void GameObject::cleanUpComponents()
{
	for(auto component : components)
	{
		delete component;
	}
}