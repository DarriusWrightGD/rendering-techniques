#pragma once
#include <Window\OpenGLWindow.h>
#include <assimp\scene.h>
#include <Camera\FirstPersonCamera.h>

// Start the concept of the model, material, and uniforms  
/*
the model knows the number of vertices that it has, also its arraybuffer 
the material will know its uniforms and the information that it points to float *
the uniform will hold the index that the uniform is located at
*/

class PointLightDemo : public OpenGLWindow
{
public:
	PointLightDemo(void);
	~PointLightDemo(void);

	virtual void initialize()override;
	virtual void update()override;
	virtual void draw() override;
	virtual void resize(int width, int height)override;

private:
	void loadModel(const char * modelName);
	glm::mat4 getPerspectiveMatrix();

	void printSceneInfo(const aiScene * scene);
	void readShaderProgram();
	void compileShader(GLuint & shader, const char * source);
	void printShaderInfo(GLuint shader);
	std::string fileToString(const char * filename);

	
	GLuint points_vbo;
	GLuint colors_vbo;
	GLuint vao;
	GLuint shaderProgram;
	glm::mat4 modelMatrix;
	GLint modelLocation;
	GLint viewLocation;
	GLint projectionLocation;
	GLint lightDirectionLocation;
	GLint lightColorLocation;
	
	GLint shineLocation;
	GLint normalMatrixLocation;
	GLint ambientColorLocation;
	GLint modelSpecularColorLocation;
	GLint modelDiffuseColorLocation;

	GLint modelViewLocation;
	GLfloat speed;
	GLfloat lastPosition;

	glm::vec3 lightColor;
	
	glm::vec3 ambientColor;

	QTimer timer;

	FirstPersonCamera camera;
	glm::vec4 lightPosition;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat3 normalMatrix;
	glm::mat4 modelView;

	int numberOfVertices;
};