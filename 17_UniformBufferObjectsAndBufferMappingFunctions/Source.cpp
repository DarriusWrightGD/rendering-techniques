#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "UniformBufferDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	UniformBufferDemo widget;
	widget.show();
	return app.exec();
}