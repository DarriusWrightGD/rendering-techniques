#include <Windows.h>
#include "Camera.h"
#include "GameTimer.h"
#include <glm\gtx\transform.hpp>
#include <QtCore\qdebug.h>

const float Camera::DEFAULT_FIELD_OF_VIEW = glm::quarter_pi<float>();
const float Camera::DEFAULT_NEAR_PLANE_DISTANCE = 0.1f;
const float Camera::DEFAULT_FAR_PLANE_DISTANCE = 1000.0f;

Camera::Camera(float aspectRatio,float fieldOfView,  float nearPlaneDistance, float farPlaneDistance):
	aspectRatio(aspectRatio), fieldOfView(fieldOfView), nearPlaneDistance(nearPlaneDistance), farPlaneDistance(farPlaneDistance),position(0.0f,0.0f,2.0f), lookAt()
{

}

Camera::~Camera()
{
}
const glm::vec3 & Camera::getPosition()const{return position;}
glm::vec3 Camera::getDirection()const{
	return normalize(lookAt - position);
}
const glm::vec3 & Camera::getLookAt()const{return lookAt;}
const glm::vec3 & Camera::getUp()const{return up;}
const glm::vec3 & Camera::getRight()const{return right;}
float Camera::getAspectRatio()const{return aspectRatio;}
float Camera::getFieldOfView()const{return fieldOfView;}
float Camera::getFarPlaneDifference()const{return farPlaneDistance;}

const glm::mat4 & Camera::getViewMatrix()const{return viewMatrix;}
const glm::mat4 & Camera::getProjectionMatrix()const{return projectionMatrix;}
const glm::mat4 & Camera::getViewProjectionMatrix()const{return projectionMatrix * viewMatrix;}

void Camera::setPosition(glm::vec3 position){this->position = position;}
void Camera::setAspectRatio(float aspectRatio){this->aspectRatio = aspectRatio;}
void Camera::initialize()
{
	updateProjectionMatrix();
	reset();
}
void Camera::update()
{
	updateViewMatrix();
}
void Camera::updateViewMatrix()
{
	viewMatrix = glm::lookAt(position, lookAt, up);
}
void Camera::updateProjectionMatrix()
{
	projectionMatrix = glm::perspective(fieldOfView,aspectRatio,nearPlaneDistance, farPlaneDistance);
}
void Camera::reset()
{
	position = glm::vec3();
	//direction = glm::vec3(0,0,-1);
	lookAt = glm::vec3();
	up = glm::vec3(0,1,0);
	right = glm::vec3(1,0,0);
	updateViewMatrix();
}

void Camera::applyRotation(glm::mat3 rotationMatrix)
{
	glm::vec3 direction = getDirection();
	direction = glm::normalize(rotationMatrix  * direction);
	up = glm::normalize(rotationMatrix * up);
	right = glm::cross(up,direction);
	up = glm::cross(direction,right);
}



//Camera::Camera(void) :position(0.0f,0.0f,2.0f), yaw(0.0f), speed(1.0f) , rotationSpeed(0.1f)
//{
//}
//
//Camera::Camera(glm::vec3 position,float speed, float yaw , float rotationSpeed)
//{
//
//}
//
//void Camera::update()
//{
//	checkKeys();
//}
//
//void Camera::checkKeys()
//{
//	if(GetAsyncKeyState('A'))
//	{
//		position.x -= speed * GAME_TIMER.delta();
//	}
//	if(GetAsyncKeyState('D'))
//	{
//		position.x += speed * GAME_TIMER.delta();
//	}
//	if(GetAsyncKeyState('W'))
//	{
//		position.z -= speed * GAME_TIMER.delta();
//	}
//	if(GetAsyncKeyState('S'))
//	{
//		position.z += speed * GAME_TIMER.delta();
//	}
//	if(GetAsyncKeyState(VK_NEXT))
//	{
//		position.y -= speed * GAME_TIMER.delta();
//	}
//	if(GetAsyncKeyState(VK_PRIOR))
//	{
//		position.y += speed * GAME_TIMER.delta();
//	}
//
//	if(GetAsyncKeyState(VK_LEFT))
//	{
//		yaw -= speed * GAME_TIMER.delta();
//	}
//	if(GetAsyncKeyState(VK_RIGHT))
//	{
//		yaw += speed * GAME_TIMER.delta();
//	}
//}
//
//
//void Camera::setSpeed(float speed)
//{
//	this->speed = speed;
//}
//void Camera::setYaw(float yaw)
//{
//	this->yaw = yaw; 
//}
//void Camera::setPosition(glm::vec3 position)
//{
//	this->position = position;
//}

//#include "Camera.h"
//
//const float Camera::CAMERA_SPEED = 1.0f;
//const float Camera::MOVEMENT_SPEED = 0.1f;
//
//Camera::Camera(void) : viewDirection(0.0f,0.0f,-1.0f),UP(0.0f,1.0f,0.0f),position(0,0,10)
//{
//}
//
//
//Camera::~Camera(void)
//{
//}
//
//void Camera::mouseUpdate(const glm::vec2& mousePosition)
//{
//	glm::vec2 newmousePosition = glm::vec2(mousePosition.x,-mousePosition.y);
//	glm::vec2 mouseDelta = newmousePosition - oldMousePosition;
//
//	if(!(glm::length(mouseDelta) > 10.0f))
//	{
//		viewDirection = glm::mat3(glm::rotate(CAMERA_SPEED* mouseDelta.x,UP)) * viewDirection;
//		viewDirection = glm::mat3(glm::rotate(CAMERA_SPEED * mouseDelta.y,glm::cross(viewDirection,UP))) * viewDirection;
//	}
//
//	oldMousePosition = newmousePosition;
//
//}
//
//void Camera:: moveForwards()
//{
//	position += MOVEMENT_SPEED * viewDirection;
//}
//void Camera:: moveBackwards()
//{
//	position -= MOVEMENT_SPEED * viewDirection;
//}
//void Camera:: moveLeft()
//{
//	glm::vec3 strafeDirection = glm::cross(viewDirection,UP);
//	position -= MOVEMENT_SPEED * strafeDirection;
//}
//void Camera:: moveRight()
//{
//	glm::vec3 strafeDirection = glm::cross(viewDirection,UP);
//	position += MOVEMENT_SPEED * strafeDirection;
//}
//void Camera:: moveUp()
//{
//	position -= MOVEMENT_SPEED * UP;
//}
//void Camera:: moveDown()
//{
//	position += MOVEMENT_SPEED * UP;
//}
