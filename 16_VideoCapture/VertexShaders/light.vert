#version 430
in layout (location = 0) vec3 vertexPosition;
in layout (location = 1) vec3 vertexNormal;
in layout (location = 2) vec2 vertexTexture;

uniform vec3 lightDirection;
uniform mat4 model;
uniform mat4 view;
uniform mat4 modelView;
uniform mat4 projection;
uniform mat3 normalMatrix;
out vec3 normal;
out vec3 position;
out vec2 textureCoord;

void main()
{
	position = vec3( modelView * vec4(vertexPosition,1.0));
	normal = vec3(normalize(normalMatrix *  vertexNormal));
	textureCoord = vertexTexture;
	
	gl_Position = projection * vec4(position, 1.0);
}
