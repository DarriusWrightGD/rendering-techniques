#include "RenderApp.h"
#include "QtWidgets/qapplication.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);

	RenderApp window;
	window.show();

	return app.exec();
}