﻿#pragma once
#include "RenderAppGLWindow.h"
#include <QtWidgets/qmainwindow.h>
#include <QtWidgets/qlayout.h>
#include "GameObjectPanel.h"
#include <QtQml/qqml.h>
#include <QtQml/qqmlapplicationengine.h>
#include <QtWidgets/qmenu.h>

class RenderApp : public QMainWindow
{

public:
	RenderApp();
private:
	QQmlApplicationEngine engine;
	GameObjectPanel gameObjectPanel;
	QWidget * mainWidget;
	QHBoxLayout * mainLayout;
	RenderAppGLWindow glWindow;

	QMenu * fileMenu;
	QMenu * addMenu;

	QAction * addModelAction;

	void setUpLayout();
	void setUpMenu();
	
	void showAddModelDialog();

};
