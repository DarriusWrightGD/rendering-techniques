#version 430

uniform vec3 particleColor;
out vec4 color;

void main()
{
	color = vec4(particleColor, 0.7f);
}