#pragma once

#include "Camera.h"
#include <Windows.h>
#include "Mouse.h"
#include "Keyboard.h"
class FirstPersonCamera : public Camera
{
public:
	FirstPersonCamera(float aspectRatio,float fieldOfView = DEFAULT_FIELD_OF_VIEW,  float nearPlaneDistance = DEFAULT_NEAR_PLANE_DISTANCE, float farPlaneDistance = DEFAULT_FAR_PLANE_DISTANCE);
	virtual ~FirstPersonCamera(void);
	void setMouse(Mouse & mouse);
	void setKeyboard(Keyboard & keyboard);
	virtual void initialize()override;
	virtual void update()override;

	static const float DEFAULT_ROTATION_RATE;
	static const float DEFAULT_MOVEMENT_RATE;
	static const float DEFAULT_MOUSE_SENSITIVITY;
protected:
	virtual void handleKeyInput();
	virtual void handleMouseInput();
	Mouse * mouse;
	Keyboard * keyboard;
	float mouseSensitivity;
	float rotateRate;
	float movementRate;
	float pitch;
	float yaw;
};

