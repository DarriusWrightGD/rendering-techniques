#include "GuiComponent.h"
#include <GLContext.h>
glm::vec2 fullScreenPos [] = 
{
	glm::vec2(-1,-1  ),
	glm::vec2(1,-1	 ),
	glm::vec2(1,1	 ),
	glm::vec2(1,1	 ),
	glm::vec2(-1,1	 ),
	glm::vec2(-1,-1  )
};


glm::vec2 textureCoords [] = 
{
	glm::vec2(0.0, 0.0),
	glm::vec2(1.0, 0.0),
	glm::vec2(1.0, 1.0),
	glm::vec2(1.0, 1.0),
	glm::vec2(0.0, 1.0),
	glm::vec2(0.0, 0.0),
};

RTTI_DEFINITIONS(GuiComponent);

GuiComponent::GuiComponent(GameObject * gameObject, const char * textureFile,glm::vec2 start, glm::vec2 end)  
	: RenderComponent(gameObject,NUMBER_OF_VERTICES), start(start), end(end)
{
	setUp();
	addTexture(textureFile);
}
GuiComponent::GuiComponent(GameObject * gameObject, GLuint textureId,glm::vec2 start, glm::vec2 end) 
	: RenderComponent(gameObject,NUMBER_OF_VERTICES), start(start), end(end)
{
	setUp();
	addTexture(textureId);
}

GuiComponent::~GuiComponent(void)
{

}

void GuiComponent::setUp()
{

	glm::vec2 * coords = new glm::vec2[NUMBER_OF_VERTICES];

	coords[0] = getLocation(glm::vec2(-1,-1), glm::vec2(start.x,end.y));//glm::vec2(-1 * start.x, -1 * start.y);
	coords[1] = getLocation(glm::vec2(1,-1), end);//glm::vec2(1 * end.x,-1 * end.y);
	coords[2] = getLocation(glm::vec2(1,1),end);//glm::vec2(1 * end.x,1* end.y);
	coords[3] = getLocation(glm::vec2(1,1),end);//glm::vec2(1 * end.x,1* end.y);
	coords[4] = getLocation(glm::vec2(-1,1), glm::vec2(start.x,end.y));//glm::vec2(-1* start.x,1* start.y);
	coords[5] = getLocation(glm::vec2(-1,-1), glm::vec2(start.x,end.y));//glm::vec2(-1* start.x,-1* start.y);


	//coords[0] = glm::vec2(-1,-1)  ;
	//coords[1] = glm::vec2(1,-1)   ;
	//coords[2] = glm::vec2(1,1)	  ;
	//coords[3] = glm::vec2(1,1)	  ;
	//coords[4] = glm::vec2(-1,1)   ;
	//coords[5] = glm::vec2(-1,-1)  ;

	GLuint vboV;
	GLuint vboT;
	GL->glGenBuffers(1,&vboV);
	GL->glBindBuffer(GL_ARRAY_BUFFER, vboV);
	GL->glBufferData(GL_ARRAY_BUFFER,sizeof(float) * 2 * NUMBER_OF_VERTICES, &coords[0].x,GL_STATIC_DRAW);
	GL->glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,0,NULL);
	GL->glEnableVertexAttribArray(0);

	GL->glGenBuffers(1,&vboT);
	GL->glBindBuffer(GL_ARRAY_BUFFER, vboT);
	GL->glBufferData(GL_ARRAY_BUFFER,sizeof(textureCoords), &textureCoords[0].x,GL_STATIC_DRAW);
	GL->glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,0,NULL);
	GL->glEnableVertexAttribArray(1);

	delete coords;
}

glm::vec2 GuiComponent::getLocation(glm::vec2 coordinate, glm::vec2 percentage)
{
	float x = (coordinate.x + 1.0f);
	x *= percentage.x;
	x -=1.0f;



	//float x = 2.0f * percentage.x -1.0f;
	//float y = 2.0f * percentage.y -1.0f;
	float y = (coordinate.y + 1.0f);
	y *=  (percentage.y);
	y -=1.0f;

	glm::vec2 value = glm::vec2(x,y);
	//return glm::vec2((coordinate.x + 1.0f) * percentage.x - 1,(coordinate.y + 1.0f) * percentage.y - 1);
	return value;//glm::vec2((coordinate.x + 1.0f) * percentage.x - 1,(coordinate.y + 1.0f) * percentage.y - 1);
}

	//glm::vec2(-1,-1  ),
	//glm::vec2(1,-1	 ),
	//glm::vec2(1,1	 ),
	//glm::vec2(1,1	 ),
	//glm::vec2(-1,1	 ),
	//glm::vec2(-1,-1  )