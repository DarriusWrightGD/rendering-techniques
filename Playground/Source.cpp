#include "PlaygroundWindow.h"
#include <QtWidgets/qapplication.h>

int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	PlaygroundWindow window;
	window.show();

	return app.exec();
}