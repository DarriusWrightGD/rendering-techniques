#pragma once 
#include <glm\glm.hpp>
#include <Math\Math.h>
#include "cuda_runtime.h"
#
#include "cuda.h"
#include <Primitives\Ray.h>
struct Sphere
{
	glm::vec3 color;
	float radius;
	glm::vec3 position;

	 __host__ __device__ float hit (float ox, float oy)
	{
		float dx = ox - position.x;
		float dy = oy - position.y;
		if(dx * dx + dy*dy < radius * radius)
		{
			float delta = sqrtf(radius * radius - dx * dx + dy*dy);
			return delta + position.z;
		}

		return -infinity_f();

	}



	 __device__ __host__ float hit (glm::vec3 origin, glm::vec3 direction, glm::vec3 * normal)
	{
		
		//to find out if the ray and sphere intersets the distance between the ray and the sphere should be calculated
		glm::vec3 originToShape(origin - position);
		float a = dot(direction, direction);
		float b = 2.0f * dot(originToShape, direction);
		float c = dot(originToShape, originToShape) - radius * radius;
		float disc = b*b - 4.0f * a * c;
		float t ;

		if(disc > 0.0f)
		{
			float e = sqrtf(disc);
			float denom = 2.0f * a;
			t = (-b-e)/denom;

			if(t > epsilion_f())	
			{
				//s.normal = (float3) (((ray.origin - position) + t * ray.direction)/ radius);
				//s.intersectionPoint = (ray.origin - position) + t * ray.direction;
				//s.hasIntersection = true;
				//s.distanceToIntersection = t;
				glm::vec3 intersectionPoint(origin + t*direction);
				*normal = glm::normalize(intersectionPoint - position);
				return t;
			}

			t = (-b + e) / denom;
			if(t > epsilion_f())
			{
				//s.normal = (float3)(((ray.origin.xyz - position) + t * ray.direction.xyz)/radius);
				//s.hasIntersection = true;
				//s.intersectionPoint = (ray.origin.xyz - position) + t * ray.direction.xyz;
				//s.distanceToIntersection = t;
				glm::vec3 intersectionPoint(origin + t*direction);
				*normal = glm::normalize(intersectionPoint - position);
				return t;
			}
		}

		return -infinity_f();

	}
};