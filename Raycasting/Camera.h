#include <glm/gtx/transform.hpp>
#include <glm/glm.hpp>
class Camera
{

public:
	Camera(glm::vec3 position,float speed, float yaw );
	Camera();
	void update();
	glm::mat4 getViewMatrix()
	{
		glm::mat4 translation = glm::translate(-position);
		glm::mat4 rotation = glm::rotate(-yaw, glm::vec3(0.0f,1.0f,0.0f));
		return rotation * translation;
	}

	void setSpeed(float speed);
	void setYaw(float yaw);
	void setPosition(glm::vec3 position);

private:
	void checkKeys();
	glm::vec3 position;
	float speed;
	float yaw;
};

//#pragma once
//#include <glm/glm.hpp>
//#include <glm\gtx\transform.hpp>
//
//#pragma warning(disable : 4512)
//class Camera
//{
//
//
//
//	glm::vec2 oldMousePosition;
//	static const float CAMERA_SPEED;
//	static const float MOVEMENT_SPEED;
//	const glm::vec3 UP;
//
//public:
//	 Camera(void);
//	 ~Camera(void);
//	glm::vec3 position;
//	glm::vec3 viewDirection;
//	void mouseUpdate(const glm::vec2 & newMousePosition);
//
//	void moveForwards();
//	void moveBackwards();
//	void moveLeft();
//	void moveRight();
//	void moveUp();
//	void moveDown();
//
//	inline glm::mat4 getWorldToViewMatrix()const
//	{
//		return glm::lookAt(position,position+ viewDirection,UP);
//	}
//};
