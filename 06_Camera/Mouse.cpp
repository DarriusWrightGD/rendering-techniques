#include "Mouse.h"
#include <QtCore\qdebug.h>

Mouse::Mouse(void) : leftButtonDown(false) , rightButtonDown(false)
{
}


Mouse::~Mouse(void)
{
}

void  Mouse::setPosition(glm::vec2 position)
{
	static bool initialSet = false;
	if(initialSet)
	{
		delta = position - this->position;
	}
	else
	{
		initialSet = true;
	}

	this->position = position;
}
const glm::vec2 & Mouse::getPosition()const
{
	return position;
}

const glm::vec2 & Mouse::getDelta()const
{
	return delta;
}

bool Mouse::isRightButtonDown()const
{
	return rightButtonDown;
}
bool Mouse::isLeftButtonDown()const
{
	return leftButtonDown;
}
void Mouse::setRightButtonDown(bool isDown)
{
	rightButtonDown = isDown;
}
void Mouse::setLeftButtonDown(bool isDown)
{
	leftButtonDown = isDown;
}