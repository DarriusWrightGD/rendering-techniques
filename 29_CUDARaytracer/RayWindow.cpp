#include "RayWindow.h"
#include <QtCore\qdebug.h>
#include <GLContext.h>
RayWindow::RayWindow(int width, int height) : windowHeight(height), windowWidth(width), time(0.0f),
	numberOfSpheres(20),title("GPU Ray Tracer ")
{
	setFixedSize(width,height);
	this->setWindowTitle(title);
}


RayWindow::~RayWindow(void)
{ 
	freeCudaMem();
}

void RayWindow::initialize()
{
	GL->glGenBuffers(1,&bufferObj);
	GL->glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,bufferObj);
	GL->glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, windowWidth *windowHeight * 4, NULL, GL_DYNAMIC_DRAW_ARB);	
	initializeRayTracer(bufferObj);
}



void RayWindow::update()
{

}

void RayWindow::resize(int width,int height)
{

}

void RayWindow::draw()
{
	trace();
}


void RayWindow::trace()
{
	elapseTimer.start();
	drawRays( time, bufferObj);
	GL->glDrawPixels(windowWidth,windowHeight,GL_RGBA,GL_UNSIGNED_BYTE,0);
	float eTime = elapseTimer.elapsed();
	time +=eTime;
	setWindowTitle(title + QString::number(1.0f/(eTime/1000.0f)) + " fps");
}
