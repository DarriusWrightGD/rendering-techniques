#version 430
uniform sampler2D imageTexture;

in vec2 textureCoord;
out vec4 fragColor;

void main()
{
	fragColor = texture(imageTexture,textureCoord);
}