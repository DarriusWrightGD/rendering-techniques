#pragma once
#include <QtCore\qelapsedtimer.h>

#define GAME_TIMER GameTimer::getInstance()
class GameTimer
{
public:
	~GameTimer(void);
	static GameTimer & getInstance()
	{
		static GameTimer gameTimer;
		return gameTimer;
	}
	
	float delta();
	void stop();
	void restart();


private:
	QElapsedTimer deltaTimer;
	GameTimer(void);
	float elapsedTime;


};

