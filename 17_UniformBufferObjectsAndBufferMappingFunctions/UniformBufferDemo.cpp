#include "UniformBufferDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>

#include <stdlib.h>
#include <GLContext.h>

UniformBufferDemo::UniformBufferDemo(void): lightPosition(glm::vec4(glm::normalize(glm::vec3(.2,.2,-1)),0.0f)),
	lightColor(0.4f,0.4f,0.9f),diffuse(0.2f,0.2f,0.9f,1.0f), specular(0.9f,0.0f,0.0f, 1.0f), ambientColor(0.3f,0.1,0.2), camera((float)width()/height())
{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);

}


UniformBufferDemo::~UniformBufferDemo(void)
{
	delete cubeRenderComponent;
	delete monkeyRenderComponent;
	delete cubeObject;
	delete monkeyObject;
}

void UniformBufferDemo::keyPressEvent(QKeyEvent * e)
{
	if((e->key() == Qt::Key_F10))
	{
		unsigned char * buffer = new unsigned char[width() * height() * 3];
		GL->glReadPixels(0,0,width(), height(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
		QImage image(buffer,width(),height(),QImage::Format::Format_RGB888);
		image = image.mirrored();
		image.save("glSaved.jpg");
		delete [] buffer;

	}
}

void UniformBufferDemo::initialize()
{
	cubeObject = new GameObject();
	cubeObject->getTransform()->setPosition(glm::vec3(0,0,-10));
	cubeRenderComponent = new RenderComponent(cubeObject,"../Models/cube.obj");
	cubeObject->add(cubeRenderComponent);

	monkeyObject = new GameObject();
	monkeyObject->setPosition(glm::vec3(2,0,-13));
	monkeyRenderComponent = new RenderComponent(monkeyObject, "../Models/monkey.obj");
	cubeObject->add(monkeyRenderComponent);

	cubeRenderComponent->addShaderFile("VertexShaders/texture.vert", GL_VERTEX_SHADER);
	cubeRenderComponent->addShaderFile("FragmentShaders/texture.frag", GL_FRAGMENT_SHADER);
	cubeRenderComponent->buildProgram();



	monkeyRenderComponent->addShaderFile("VertexShaders/light.vert", GL_VERTEX_SHADER);
	monkeyRenderComponent->addShaderFile("FragmentShaders/light.frag", GL_FRAGMENT_SHADER);
	monkeyRenderComponent->buildProgram();

	monkeyRenderComponent->addUniform("model", MAT4, &monkeyObject->getTransform()->getTransform()[0][0]);
	monkeyRenderComponent->addUniform("view", MAT4, &view[0][0]);
	monkeyRenderComponent->addUniform("normalMatrix",MAT3,&normalMatrix[0][0]);
	monkeyRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	monkeyRenderComponent->addUniform("light.color",VEC3,&lightColor[0]);
	monkeyRenderComponent->addUniform("material.diffuse",VEC4,&diffuse[0]);
	monkeyRenderComponent->addUniform("material.specular",VEC4,&specular[0]);
	monkeyRenderComponent->addUniform("ambient",VEC3,&ambientColor[0]);
	monkeyRenderComponent->addUniform("light.position",VEC4,&lightPosition[0]);

	cubeRenderComponent->addUniform("model", MAT4, &cubeObject->getTransform()->getTransform()[0][0]);
	cubeRenderComponent->addUniform("view", MAT4, &view[0][0]);
	cubeRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	cubeRenderComponent->addTexture("../Images/brick.png");

	GL->glGenBuffers(1, &cameraBuffer);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,cameraBuffer);
	GL->glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * 2, NULL, GL_DYNAMIC_DRAW);

	int blockId = 0;
	monkeyUniformBlockIndex = GL->glGetUniformBlockIndex(monkeyRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(monkeyRenderComponent->getProgramId(), monkeyUniformBlockIndex,blockId);    
	boxUniformBlockIndex = GL->glGetUniformBlockIndex(cubeRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(cubeRenderComponent->getProgramId(), boxUniformBlockIndex,blockId);    

	GL->glBindBufferBase(GL_UNIFORM_BUFFER, blockId, cameraBuffer);
	float * cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&projection[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);


}

void UniformBufferDemo::update()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();

	//GL->glUniform1i(textureLocation,0);

	const float lightSpeed = 2.0f;

	if(GetAsyncKeyState('J'))
	{
		lightPosition.x -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('L'))
	{
		lightPosition.x += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('P'))
	{
		lightPosition.z -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('O'))
	{
		lightPosition.z += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('K'))
	{
		lightPosition.y -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('I'))
	{
		lightPosition.y += lightSpeed * GAME_TIMER.delta();
	}

	view = camera.getViewMatrix();
	camera.update();

	lightPosition = glm::vec4(glm::normalize(glm::vec3(lightPosition)),0.0);
	cubeObject->update();
	monkeyObject->update();


	repaint();
}
void UniformBufferDemo::draw() 
{
	GAME_TIMER.restart();
    
	GL->glBindBufferBase(GL_UNIFORM_BUFFER, 0, cameraBuffer);
	float * cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&view[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);

	cubeObject->draw();
	monkeyObject->draw();
}
void UniformBufferDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}
