#version 430

in vec3 fragNormal;
in vec3 fragPosition;

struct Light
{
	vec3 Ls;
	vec3 Ld;
	vec3 La;
	vec3 position;
};

struct Material
{
	vec3 Ks;
	vec3 Kd;
	vec3 Ka;
};
uniform Light light;
uniform Material material;
float specularExponent = 20.0f;

vec4 ads()
{
	vec3 position = fragPosition;
	vec3 normal = normalize(fragNormal);
	vec3 Ia = light.La * material.Ka;
	vec3 lightVector = normalize(light.position - position);

	float diffuseComponent = max(dot(lightVector, normal), 0.0);
	vec3 Id = light.Ld * material.Kd * diffuseComponent;

	vec3 reflection = reflect(-lightVector, normal);
	vec3 surfaceToView = normalize(-light.position);
	float dotSpec = max(dot(reflection, surfaceToView), 0.0);
	float specFactor = pow(dotSpec, specularExponent);

	//specular is the light reflected directly by the surface, this refers to how much of the surface is like a mirror

	vec3 Is = light.Ls * material.Ks * specFactor;
	return vec4(Ia + Id + Is ,1.0);
}


in vec4 fColor;
out vec4 color;
//fix me up, lighting also test putting everything in view space
void main()
{
	color = ads();
}