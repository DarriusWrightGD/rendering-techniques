#version 430
in vec3 vp; 
in vec2 vt;
out vec2 textureCoord;

void main()
{
	textureCoord = vt;
	gl_Position = vec4(vp,1.0);
}