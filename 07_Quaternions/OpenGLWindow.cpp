#include "OpenGLWindow.h"
#include <iostream>
#include <fstream>
#include <ostream>

GLfloat points [] =  {0.0f,0.5f,0.0f,0.5f,-0.5f,0.0f,-0.5f,-0.5f,0.0f};
GLfloat colors []  = {1.0f, 0.0f, 0.0f, 0.0f ,1.0f, 0.0f,0.0f,0.0f,1.0f};
//Quaternions Start Guide
/*
1. Describe a rotation by making up an arbitrary axis and an angle around that axis
2. Generate a quaternion from the angle and the axis
3. multiply or interpolate quaternions to get combined or in-between rotations, respectively
4. normalize the quaternion
5. convert the quaternion to a matrix

*******************************************************
Warning (SLERP) Spherical interpolation can be a bit expensive so only use when necessary, a good idea would be to do something 
similar to unity with there quaternion class and then create checks that determine if the quaternion is needed and then create one if necessary.
But all the user will know is that they obtained a quaternion and not if there was any SLERP performed. 
*******************************************************

Quaternions used for rotations are known as versors

*/


OpenGLWindow::OpenGLWindow() : speed(1.0f), lastPosition(0.0f)
{
	setUpFormat();
}


OpenGLWindow::~OpenGLWindow(void) 
{

}


void OpenGLWindow::initializeGL()
{

	initializeOpenGLFunctions();
	glClearColor(0.4f,0.3f,0.2f,1.0f);

	glGenBuffers(1,&points_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

	glGenBuffers(1,&colors_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

	glGenVertexArrays(1,&vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, points_vbo);

	glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 0, NULL);
	glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);

	glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);	

	readShaderProgram();
	GAME_TIMER.restart();
	connect(&timer,&QTimer::timeout,this,&OpenGLWindow::glUpdate);
	timer.start();

	projection = glm::perspective(60.0f,(float)width()/ height(), 0.1f, 100.0f);

}

void OpenGLWindow::resizeGL(int w, int h)
{
	projection = glm::perspective(60.0f,(float)width()/ height(), 0.1f, 100.0f);
}

void OpenGLWindow::glUpdate()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();
	glUseProgram(shaderProgram);

	if(fabs(lastPosition) > 1.0f)
	{
		speed = -speed;
	}
	view = camera.getViewMatrix();
	camera.update();
	lastPosition += speed * deltaTime;
	triangleMatrix = glm::translate(glm::vec3(lastPosition,0.0f,0.0f));
	glUniformMatrix4fv(modelLocation,1,GL_FALSE, &triangleMatrix[0][0]);
	glUniformMatrix4fv(viewLocation,1,GL_FALSE, &view[0][0]);
	glUniformMatrix4fv(projectionLocation,1,GL_FALSE, &projection[0][0]);
	
	repaint();
	
}

void OpenGLWindow::keyPressEvent(QKeyEvent * e)
{


}

void OpenGLWindow::keyReleaseEvent(QKeyEvent * e)
{

}

void OpenGLWindow::paintGL()
{
	GAME_TIMER.restart();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);



	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES,0,3);


}

void OpenGLWindow::setUpFormat()
{
	QSurfaceFormat format;
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setSamples(4);
	format.setVersion(4, 3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	setFormat(format); 
}

void OpenGLWindow::compileShader(GLuint & shader, const char * source)
{
	int params = -1;
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}
}

void OpenGLWindow::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}

void OpenGLWindow::readShaderProgram()
{
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);

	compileShader(vertexShader,fileToString("VertexShaders/color.vert").c_str());

	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	compileShader(fragmentShader, fileToString("FragmentShaders/color.frag").c_str());

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, fragmentShader);
	glAttachShader(shaderProgram,vertexShader);

	glLinkProgram(shaderProgram);

	int params = -1;

	glGetProgramiv(shaderProgram , GL_LINK_STATUS, &params);
	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : could not link shader program GL index %u\n", shaderProgram);
		printShaderInfo(shaderProgram);
		system("pause");
		exit(-1);
	}
	modelLocation = glGetUniformLocation(shaderProgram, "model");
	viewLocation = glGetUniformLocation(shaderProgram, "view");
	projectionLocation = glGetUniformLocation(shaderProgram, "projection");
	triangleMatrix = glm::translate(glm::vec3(0.5f,0.0f,0.0f));
	glUseProgram(shaderProgram);
	glUniformMatrix4fv(modelLocation,1,GL_FALSE, &triangleMatrix[0][0]);
}


std::string OpenGLWindow::fileToString(const char * filename)
{
	return std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>());
}