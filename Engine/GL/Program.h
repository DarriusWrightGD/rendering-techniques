#pragma once
#include <GLContext.h>
#include <Uniform.h>
#include <map>
#include <Export.h>
class Program
{
public:
	ENGINE_SHARED Program(void);
	ENGINE_SHARED ~Program(void);
	ENGINE_SHARED void addUniform(const char * name,UniformType type, float * data);
	ENGINE_SHARED void addTexture(const char * imagePath);
	ENGINE_SHARED void addTexture(GLuint textureId);
	ENGINE_SHARED void addShaderFile(const std::string & filename, GLenum type);
	ENGINE_SHARED void addShaderSource(const std::string & source, GLenum type);
	ENGINE_SHARED void buildProgram();
	ENGINE_SHARED GLuint getProgram();
	ENGINE_SHARED void updateUniforms();
private:
	void setUniform(Uniform uniform, float * value, int count = 1, bool transpose = false);
	//throw exception instead of printing error messages later on
	void printShaderInfo(GLuint shader);
	std::map<Uniform, float*> uniforms;
	std::vector<GLuint> textures;
	std::vector<GLuint> shaders;
	GLuint numberOfTextures;
	GLuint programId;


};


