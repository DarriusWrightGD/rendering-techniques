#pragma once

#include <QtWidgets/qwidget.h>
#include <QtQml/qqml.h>
#include <QtQml/qqmlengine.h>
#include <QtQml/qqmlcomponent.h>
#include <QtQuick/qquickview.h>
#include <QtQuick/qquickitem.h>
#include <QtWidgets/qlayout.h>

class Panel : public QWidget
{
public:
	Panel(QQmlEngine * engine,const char * qmlFile);
	~Panel(void);

protected:
	QQmlEngine * engine;
	QQuickView * view;
	QQmlComponent component;
	QWidget * container;
	QQuickItem * quickItem;
	QHBoxLayout * mainLayout;
	const char * qmlFile;
};

