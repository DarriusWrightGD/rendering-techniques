#include "TextureSlicingDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>

#include <stdlib.h>
#include <GLContext.h>

////unit cube vertices
glm::vec3 vertexList[8] = {glm::vec3(-0.5,-0.5,-0.5),
	glm::vec3( 0.5,-0.5,-0.5),
	glm::vec3(0.5, 0.5,-0.5),
	glm::vec3(-0.5, 0.5,-0.5),
	glm::vec3(-0.5,-0.5, 0.5),
	glm::vec3(0.5,-0.5, 0.5),
	glm::vec3( 0.5, 0.5, 0.5),
	glm::vec3(-0.5, 0.5, 0.5)};
//
////unit cube edges
int edgeList[8][12] = {
	{ 0,1,5,6,   4,8,11,9,  3,7,2,10 }, // v0 is front
	{ 0,4,3,11,  1,2,6,7,   5,9,8,10 }, // v1 is front
	{ 1,5,0,8,   2,3,7,4,   6,10,9,11}, // v2 is front
	{ 7,11,10,8, 2,6,1,9,   3,0,4,5  }, // v3 is front
	{ 8,5,9,1,   11,10,7,6, 4,3,0,2  }, // v4 is front
	{ 9,6,10,2,  8,11,4,7,  5,0,1,3  }, // v5 is front
	{ 9,8,5,4,   6,1,2,0,   10,7,11,3}, // v6 is front
	{ 10,9,6,5,  7,2,3,1,   11,4,8,0 }  // v7 is front
};
int edges[12][2]= {{0,1},{1,2},{2,3},{3,0},{0,4},{1,5},{2,6},{3,7},{4,5},{5,6},{6,7},{7,4}};

TextureSlicingDemo::TextureSlicingDemo(void): lightPosition(glm::vec4(glm::normalize(glm::vec3(.2,.2,-1)),0.0f)),
	lightColor(0.4f,0.4f,0.9f),diffuse(0.2f,0.2f,0.9f,1.0f), specular(0.9f,0.0f,0.0f, 1.0f), ambientColor(0.3f,0.1,0.2), camera((float)width()/height()),
	numberOfSlices(512), model( glm::translate(glm::vec3(0.0f,0.0f,-3.0f)) * glm::rotate(70.0f,glm::vec3(0.0f,1.0f,0.0f)))
{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);

}


TextureSlicingDemo::~TextureSlicingDemo(void)
{
	//delete colorGuiComponent;
	//delete depthGuiComponent;
	//delete cubeRenderComponent;
	//delete monkeyRenderComponent;
	//delete cubeObject;
	//delete monkeyObject;
}

void TextureSlicingDemo::keyPressEvent(QKeyEvent * e)
{
	if((e->key() == Qt::Key_F10))
	{
		unsigned char * buffer = new unsigned char[width() * height() * 3];
		GL->glReadPixels(0,0,width(), height(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
		QImage image(buffer,width(),height(),QImage::Format::Format_RGB888);
		image = image.mirrored();
		image.save("glSaved.jpg");
		delete [] buffer;

	}
	else if(e->key() == Qt::Key_Space)
	{
		calculateDistance();
	}
	else if(e->key() == Qt::Key_Plus)
	{
		if(numberOfSlices < MAX_SLICES)
			numberOfSlices *=2;
		calculateDistance();
	}
	else if(e->key() == Qt::Key_Minus)
	{
		numberOfSlices /=2;
		calculateDistance();
	}
}

void TextureSlicingDemo::calculateDistance()
{
	//Get the current view direction vector then get the 
	//min/max distance of the unit cube from the camera's direction through
	// the dot product...
	maxDistance = glm::dot(camera.getDirection(),vertexList[0]);
	minDistance = maxDistance;
	int maxIndex = 0;
	
	for (int i = 1; i < 8; i++)
	{
		float distance =  glm::dot(camera.getDirection(),vertexList[i]);
		if(distance > maxDistance)
		{
			maxDistance = distance;
			maxIndex = i;
		}

		minDistance = (distance < minDistance) ? distance : minDistance;
	}

	// find the max direction of the camera and then move the min and max distance back a bit
	int maxDimensions = findAbsMax(camera.getDirection());
	minDistance -= glm::epsilon<float>();
	maxDistance += glm::epsilon<float>();

	
	glm::vec3 vecStart[12];
	glm::vec3 vecDir[12];
	float lambda[12];
	float lambda_inc[12];
	float denom = 0;
	float planeDistance = minDistance;
	float planeDistanceInc = (maxDistance - minDistance)/(float)(numberOfSlices);
	for(int i = 0; i < 12; i++)
	{
		//get the start position vertex by table lookup
		vecStart[i] = vertexList[edges[edgeList[maxIndex][i]][0]];

		//get the direction by table lookup
		vecDir[i] = vertexList[edges[edgeList[maxIndex][i]][1]]-vecStart[i];

		//do a dot of vecDir with the view direction vector
		denom = glm::dot(vecDir[i], camera.getDirection());

		//determine the plane intersection parameter (lambda) and 
		//plane intersection parameter increment (lambda_inc)
		if (1.0 + denom != 1.0) {
			lambda_inc[i] =  planeDistanceInc/denom;
			lambda[i]     = (planeDistance - glm::dot(vecStart[i],camera.getDirection()))/denom;
		} else {
			lambda[i]     = -1.0;
			lambda_inc[i] =  0.0;
		}
	}
	
	glm::vec3 intersection[6];
	float dL[12];
	uint count = 0;
	for (int i = numberOfSlices-1; i >= 0 ; i--)
	{
		for(int e = 0; e < 12; e++)
		{
			dL[e] = lambda[e] + i*lambda_inc[e];
		}

		if((dL[0]>= 0.0) && (dL[0] < 1.0))
		{
			intersection[0] = vecStart[0] + dL[0] * vecDir[0];
		}
		else if ((dL[1] >= 0.0) && (dL[1] < 1.0))	{
			intersection[0] = vecStart[1] + dL[1]*vecDir[1];
		}
		else if ((dL[3] >= 0.0) && (dL[3] < 1.0))	{
			intersection[0] = vecStart[3] + dL[3]*vecDir[3];
		}
		else continue;

		if ((dL[2] >= 0.0) && (dL[2] < 1.0)){
			intersection[1] = vecStart[2] + dL[2]*vecDir[2];
		}
		else if ((dL[0] >= 0.0) && (dL[0] < 1.0)){
			intersection[1] = vecStart[0] + dL[0]*vecDir[0];
		}
		else if ((dL[1] >= 0.0) && (dL[1] < 1.0)){
			intersection[1] = vecStart[1] + dL[1]*vecDir[1];
		} else {
			intersection[1] = vecStart[3] + dL[3]*vecDir[3];
		}

		if  ((dL[4] >= 0.0) && (dL[4] < 1.0)){
			intersection[2] = vecStart[4] + dL[4]*vecDir[4];
		}
		else if ((dL[5] >= 0.0) && (dL[5] < 1.0)){
			intersection[2] = vecStart[5] + dL[5]*vecDir[5];
		} else {
			intersection[2] = vecStart[7] + dL[7]*vecDir[7];
		}
		if	((dL[6] >= 0.0) && (dL[6] < 1.0)){
			intersection[3] = vecStart[6] + dL[6]*vecDir[6];
		}
		else if ((dL[4] >= 0.0) && (dL[4] < 1.0)){
			intersection[3] = vecStart[4] + dL[4]*vecDir[4];
		}
		else if ((dL[5] >= 0.0) && (dL[5] < 1.0)){
			intersection[3] = vecStart[5] + dL[5]*vecDir[5];
		} else {
			intersection[3] = vecStart[7] + dL[7]*vecDir[7];
		}
		if	((dL[8] >= 0.0) && (dL[8] < 1.0)){
			intersection[4] = vecStart[8] + dL[8]*vecDir[8];
		}
		else if ((dL[9] >= 0.0) && (dL[9] < 1.0)){
			intersection[4] = vecStart[9] + dL[9]*vecDir[9];
		} else {
			intersection[4] = vecStart[11] + dL[11]*vecDir[11];
		}

		if ((dL[10]>= 0.0) && (dL[10]< 1.0)){
			intersection[5] = vecStart[10] + dL[10]*vecDir[10];
		}
		else if ((dL[8] >= 0.0) && (dL[8] < 1.0)){
			intersection[5] = vecStart[8] + dL[8]*vecDir[8];
		}
		else if ((dL[9] >= 0.0) && (dL[9] < 1.0)){
			intersection[5] = vecStart[9] + dL[9]*vecDir[9];
		} else {
			intersection[5] = vecStart[11] + dL[11]*vecDir[11];
		}

		int indices[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5};

		for (int i = 0; i < 12; i++)
		{
			textureSlices[count++] = intersection[indices[i]];
		}
	}

	GL->glBindBuffer(GL_ARRAY_BUFFER, volumeVbo);
	GL->glBufferSubData(GL_ARRAY_BUFFER, 0,  sizeof(textureSlices), &(textureSlices[0].x));
}

int TextureSlicingDemo::findAbsMax(glm::vec3 vector)
{
	vector = glm::abs(vector);
	int max = (vector.x > vector.y) ? 0 : 1;
	max = (vector[max] > vector.z) ? max : 2;
	return  max;
}

void TextureSlicingDemo::addShaderFile(const std::string & filename, GLenum type)
{
	addShaderSource(std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>()), type);
}
void TextureSlicingDemo::addShaderSource(const std::string & source, GLenum type)
{
	int params = -1;
	GLuint shader = GL->glCreateShader(type);
	const char * content = source.c_str();
	GL->glShaderSource(shader, 1, &content, NULL);
	GL->glCompileShader(shader);
	GL->glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}

	shaders.push_back(shader);
}

void TextureSlicingDemo::buildProgram()
{
	program = GL->glCreateProgram();
	for(GLuint shader : shaders)
	{
		GL->glAttachShader(program,shader);
	}
	GL->glLinkProgram(program);
}


void TextureSlicingDemo::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	GL->glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}

void TextureSlicingDemo::initialize()
{
	std::ifstream infile("VolumeData/golfball.raw",std::ios_base::binary);
	if(infile.good())
	{
		GLubyte * volumeData = new GLubyte[512*256*256];
		infile.read(reinterpret_cast<char*>(volumeData),512*256*256* sizeof(GLubyte));
		infile.close();
		GL->glGenTextures(1,&volumeTexture);
		GL->glBindTexture(GL_TEXTURE_3D,volumeTexture);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S,GL_CLAMP);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T,GL_CLAMP);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R,GL_CLAMP);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_BASE_LEVEL,0);
		GL->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAX_LEVEL,4);
		GL->glTexImage3D(GL_TEXTURE_3D,0,GL_RED,512,256,256,0,GL_RED,GL_UNSIGNED_BYTE,volumeData);
		GL->glGenerateMipmap(GL_TEXTURE_3D);
		delete volumeData;
	}

	GL->glGenVertexArrays(1,&volumeVao);
	GL->glBindVertexArray(volumeVao);
	GL->glGenBuffers(1,&volumeVbo);
	GL->glBindBuffer(GL_ARRAY_BUFFER,volumeVbo);
	GL->glBufferData(GL_ARRAY_BUFFER,sizeof(textureSlices),0,GL_DYNAMIC_DRAW);
	GL->glEnableVertexAttribArray(0);
	GL->glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);
	GL->glBindVertexArray(0);


	calculateDistance();
	addShaderFile("VertexShaders/slicing.vert", GL_VERTEX_SHADER);
	addShaderFile("FragmentShaders/slicing.frag", GL_FRAGMENT_SHADER);
	buildProgram();
	mvpLocation = GL->glGetUniformLocation(program,"MVP");
	//GL->glBindBuffer(GL_ARRAY_BUFFER,volumeVbo);
	//GL->glBufferSubData(GL_ARRAY_BUFFER,0,sizeof(textureSlices), &textureSlices[0].x);

	GL->glEnable(GL_BLEND);
	GL->glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	GL->glBindVertexArray(volumeVao);
}

void TextureSlicingDemo::update()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();

	//GL->glUniform1i(textureLocation,0);

	const float lightSpeed = 2.0f;

	if(GetAsyncKeyState('J'))
	{
		lightPosition.x -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('L'))
	{
		lightPosition.x += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('P'))
	{
		lightPosition.z -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('O'))
	{
		lightPosition.z += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('K'))
	{
		lightPosition.y -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('I'))
	{
		lightPosition.y += lightSpeed * GAME_TIMER.delta();
	}

	view = camera.getViewMatrix();
	camera.update();

	mvp = camera.getProjectionMatrix()* view * model;
	lightPosition = glm::vec4(glm::normalize(glm::vec3(lightPosition)),0.0);
	//cubeObject->update();
	//monkeyObject->update();
	//screenObject->update();


	repaint();
}
void TextureSlicingDemo::draw() 
{
	GAME_TIMER.restart();
	GL->glUseProgram(program);
	GL->glUniformMatrix4fv(mvpLocation,1,GL_FALSE,&mvp[0][0]);	
	GL->glEnable(GL_BLEND);
	GL->glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	GL->glBindVertexArray(volumeVao);
	GL->glDrawArrays(GL_TRIANGLES,0,MAX_SLICES * 12);
}
void TextureSlicingDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}
