#pragma once
#include <Window/OpenGLWindow.h>
#include <GameObjects/GameObject.h>
#include <Components/RenderComponent.h>
#include <Camera/FirstPersonCamera.h>

class PlaygroundWindow : public OpenGLWindow
{
	
public:
	PlaygroundWindow(void);
	~PlaygroundWindow(void);
	virtual void initialize() override;
	virtual void update() override;
	virtual void draw() override;
	virtual void resize(int width, int height) override;

private:
	GameObject * monkeyGameObject;
	RenderComponent * monkeyRenderComponent;
	FirstPersonCamera camera;
	glm::mat4 projection;
	glm::mat4 view;
};

