#include <iostream>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qapplication.h>
#include "GaussianBlurDemo.h"


int main(int argc, char * argv [])
{
	QApplication app(argc, argv);
	GaussianBlurDemo widget;
	widget.show();
	return app.exec();
}