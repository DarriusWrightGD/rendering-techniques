#pragma once
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglfunctions.h>
#include <QtWidgets\qopenglwidget.h>
#include <QtOpenGLExtensions\qopenglextensions.h>
#include <QtGui\qopenglfunctions_4_3_core.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <QtCore\qtimer.h>
#include <QtCore\qelapsedtimer.h>


class OpenGLWindow :  public QOpenGLWidget , protected QOpenGLFunctions_4_3_Core
{
public:
	OpenGLWindow(void);
	~OpenGLWindow(void);

protected:
	void initializeGL()override;
	void paintGL()override;
	void resizeGL(int width, int height)override;
	void glUpdate();

private:
	void setUpFormat();
	void readShaderProgram();
	void compileShader(GLuint & shader, const char * source);
	void printShaderInfo(GLuint shader);
	std::string fileToString(const char * filename);
	
	GLuint points_vbo;
	GLuint colors_vbo;
	GLuint vao;
	GLuint shaderProgram;
	glm::mat4 triangleMatrix;
	GLint modelLocation;
	GLfloat speed;
	GLfloat lastPosition;
	QElapsedTimer deltaTimer;
	QTimer timer;
	glm::mat4 projection;

};

