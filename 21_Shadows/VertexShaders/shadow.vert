#version 430
in layout (location = 0) vec3 vertexPosition;
in layout (location = 1) vec3 vertexNormal;
in layout (location = 2) vec2 vertexTexture;

uniform vec3 lightDirection;
uniform mat4 model;
uniform mat4 modelView;
uniform mat3 normalMatrix;
uniform mat4 shadowMatrix;

out vec3 normal;
out vec3 position;
out vec2 textureCoord;
out vec4 shadowTex;


layout (std140) uniform cameraBlock
{
	mat4 projection;
	mat4 view;
};

void main()
{
	position = vec3(   model * vec4(vertexPosition,1.0));
	normal = vec3(normalize(normalMatrix * vertexNormal));
	textureCoord = vertexTexture;
	
	gl_Position = projection * view * vec4(position, 1.0);

	shadowTex = projection * shadowMatrix * vec4(position, 1.0);
	shadowTex.xyz /= shadowTex.w;
	shadowTex.xyz += 1.0;
	shadowTex.xyz *= 0.5;
}
