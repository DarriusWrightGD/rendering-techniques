#pragma once
#include <Window\OpenGLWindow.h>
#include <assimp\scene.h>
#include <Camera\FirstPersonCamera.h>
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglcolormap.h>
#include <QtGui\qopengltexture.h>
#include <map>
#include <GameObjects\GameObject.h>
#include <Components\RenderComponent.h>
#include <Components\TransformComponent.h>
// Start the concept of the model, material, and uniforms  
/*
the model knows the number of vertices that it has, also its arraybuffer 
the material will know its uniforms and the information that it points to float *
the uniform will hold the index that the uniform is located at
*/

class HardShadowsDemo : public OpenGLWindow
{
public:
	HardShadowsDemo(void);
	~HardShadowsDemo(void);

	virtual void initialize()override;
	virtual void update()override;
	virtual void draw() override;
	virtual void resize(int width, int height)override;
	virtual void keyPressEvent(QKeyEvent * e)override;
private:
	GLuint frameBuffer;
	GLuint frameBufferTexture;

	GameObject * cubeObject;
	RenderComponent * cubeRenderComponent;

	GameObject * monkeyObject;
	RenderComponent * monkeyRenderComponent;

	GameObject * floorObject;
	RenderComponent * floorRenderComponent;

	RenderComponent * depthRenderComponent;
	GLuint cameraBuffer;

	glm::mat4 shadowMatrix;

	GLuint monkeyUniformBlockIndex;
	GLuint boxUniformBlockIndex;
	FirstPersonCamera camera;

	glm::vec3 lightColor;
	glm::vec3 ambientColor;
	glm::vec4 lightPosition;

	QTimer timer;

	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat3 normalMatrix;
	glm::mat4 modelView;

	GLint numberOfBoxVertices;
	GLint numberOfMonkeyVertices;
	
};