#pragma once
#include <Components\RenderComponent.h>
#include <glm\vec2.hpp>
class GuiComponent : public RenderComponent
{
	RTTI_DECLARATIONS(GuiComponent,RenderComponent);

public:
	ENGINE_SHARED GuiComponent(GameObject * gameObject, const char * textureFile,glm::vec2 start = glm::vec2(0,0), glm::vec2 end = glm::vec2(1,1));
	ENGINE_SHARED GuiComponent(GameObject * gameObject, GLuint textureId,glm::vec2 start = glm::vec2(0,0), glm::vec2 end = glm::vec2(1,1));
	ENGINE_SHARED ~GuiComponent(void);
protected:
	ENGINE_SHARED void setUp();
	glm::vec2 start;
	glm::vec2 end;
private:
	const static GLuint NUMBER_OF_VERTICES = 6;
	glm::vec2 getLocation(glm::vec2 coordinate, glm::vec2 percentage);
};

