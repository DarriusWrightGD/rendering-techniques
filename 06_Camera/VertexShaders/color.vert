#version 430

in vec3 vertexPoint;
in vec3 vertexColor;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
out vec3 color;

void main () 
{
	color = vertexColor;
	gl_Position = projection * view * model * vec4(vertexPoint, 1.0);
}