#pragma once
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglfunctions.h>
#include <QtWidgets\qopenglwidget.h>
#include <QtOpenGLExtensions\qopenglextensions.h>
#include <QtGui\qopenglfunctions_4_3_core.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <QtCore\qtimer.h>

#include "FirstPersonCamera.h"
#include <QtGui\qevent.h>
#include "GameTimer.h"
#include "Mouse.h"
#include "Keyboard.h"

class OpenGLWindow :  public QOpenGLWidget , protected QOpenGLFunctions_4_3_Core
{
public:
	OpenGLWindow(void);
	~OpenGLWindow(void);

protected:
	void initializeGL()override;
	void paintGL()override;
	void resizeGL(int width, int height)override;
	void glUpdate();
	void keyPressEvent(QKeyEvent * e) override;
	void keyReleaseEvent(QKeyEvent * e)override;
	void mouseMoveEvent(QMouseEvent * e)override;
	void mousePressEvent(QMouseEvent * e)override;
	void mouseReleaseEvent(QMouseEvent * e)override;
private:
	glm::mat4 getPerspectiveMatrix();
	void setUpFormat();
	void readShaderProgram();
	void compileShader(GLuint & shader, const char * source);
	void printShaderInfo(GLuint shader);
	std::string fileToString(const char * filename);

	GLuint points_vbo;
	GLuint colors_vbo;
	GLuint vao;
	GLuint shaderProgram;
	glm::mat4 triangleMatrix;
	GLint modelLocation;
	GLint viewLocation;
	GLint projectionLocation;
	GLfloat speed;
	GLfloat lastPosition;
	
	QTimer timer;
	Mouse mouse;
	Keyboard keyboard;
	FirstPersonCamera camera;
	glm::mat4 view;
	glm::mat4 projection;

};

