#pragma once
#include <glm\glm.hpp>
#include "cuda_runtime.h"

struct RayCamera
{
	RayCamera(glm::vec3 position, glm::vec3 lookAt, float distance = 1000.0f) : position(position), lookAt(lookAt), up(0,1,0)
	{
		update();
		this->distance = distance;
	}

	__device__ glm::vec3 getW()
	{
		return w;
	}

	__device__ glm::vec3 getU()
	{
		return u;
	}
	__device__ glm::vec3 getV()
	{
		return v;
	}

	__host__ void update()
	{
		w = (glm::normalize(glm::vec3(position - lookAt)));
		u = (glm::normalize(glm::cross(up, glm::vec3(w))));
		v = (glm::cross(glm::vec3(w),glm::vec3(u)));
	}

	float distance;
private:


	glm::vec3 position;
	glm::vec3 lookAt;
	glm::vec3 up;
	glm::vec3 u;
	glm::vec3 v;
	glm::vec3 w;
};
