#include "ParticleSimulationDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>

#include <stdlib.h>
#include <GLContext.h>



ParticleSimulationDemo::ParticleSimulationDemo(void): camera((float)width()/height()), numberOfParticles(100,100,50),
	totalParticles((uint)(numberOfParticles.x * numberOfParticles.y * numberOfParticles.z)), blackHolePos1(-5,-30,0),
	blackHolePos2(5,30,0), particleColor(0.9f,0.4f,0.7)

{
	totalParticles = (uint)(numberOfParticles.x * numberOfParticles.y * numberOfParticles.z);
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,-100));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);

}


ParticleSimulationDemo::~ParticleSimulationDemo(void)
{

}

void ParticleSimulationDemo::keyPressEvent(QKeyEvent * e)
{
	if((e->key() == Qt::Key_F10))
	{
		auto buffer = new unsigned char[width() * height() * 3];
		GL->glReadPixels(0,0,width(), height(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
		QImage image(buffer,width(),height(),QImage::Format::Format_RGB888);
		image = image.mirrored();
		image.save("glSaved.jpg");
		delete [] buffer;
	}

}


void ParticleSimulationDemo::printComputeInfo()
{

	GLint dim[3];
	GLint count;
	qDebug() << "Compute Shader Information";
	//GL->glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_COUNT,dim);
	GL->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT,0,dim);
	GL->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT,1,&dim[1]);
	GL->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT,2,&dim[2]);
	qDebug() << "Max compute Work Group Count : " << dim[0]<< " , " << dim[1] << ", " << dim[2];
	GL->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE,0,dim);
	GL->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE,1,&dim[1]);
	GL->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE,2,&dim[2]);
	qDebug() << "Max compute work group size : " << dim[0] << " , " << dim[1] << ", " << dim[2];
	GL->glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &count);
	qDebug() << "Max compute work group invocations : " << count;
}

void ParticleSimulationDemo::initialize()
{
	printComputeInfo();

	GL->glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	GL->glEnable(GL_BLEND);
	GL->glGenVertexArrays(1,&particleArrayBuffer);
	GL->glBindVertexArray(particleArrayBuffer);

	auto bufferSize = totalParticles * sizeof(glm::vec4);
	GLuint positionBuffer;
	initialPositions.resize(totalParticles);
	
	for (int x = 0; x < numberOfParticles.x; x++)
	{
		for (int y = 0; y < numberOfParticles.y; y++)
		{
			for (int z = 0; z < numberOfParticles.z; z++)
			{
				initialPositions[x*y+z] = glm::vec4(glm::vec3(x,y,z) - numberOfParticles/2.0f,1.0f);
			}
		}
	}

	GL->glGenBuffers(1,&positionBuffer);
	GL->glBindBufferBase(GL_SHADER_STORAGE_BUFFER,0,positionBuffer);
	GL->glBufferData(GL_SHADER_STORAGE_BUFFER,bufferSize,&initialPositions[0],GL_DYNAMIC_DRAW);

	GL->glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	GL->glVertexAttribPointer(0,4,GL_FLOAT,GL_FALSE,0,0);
	GL->glEnableVertexAttribArray(0);

	GLuint velocityBuffer;
	velocities.resize(totalParticles,glm::vec4(0));
	GL->glGenBuffers(1,&velocityBuffer);
	GL->glBindBufferBase(GL_SHADER_STORAGE_BUFFER,1,velocityBuffer);
	GL->glBufferData(GL_SHADER_STORAGE_BUFFER,bufferSize,&velocities[0],GL_DYNAMIC_DRAW);

	



	computeProgram.addShaderFile("ComputeShaders/particleProcess.comp",GL_COMPUTE_SHADER);
	computeProgram.buildProgram();
	computeProgram.addUniform("blackHole1",UniformType::VEC3,&blackHolePos1[0]);
	computeProgram.addUniform("blackHole2",UniformType::VEC3,&blackHolePos2[0]);

	renderProgram.addShaderFile("VertexShaders/particle.vert",GL_VERTEX_SHADER);
	renderProgram.addShaderFile("FragmentShaders/particle.frag",GL_FRAGMENT_SHADER);
	renderProgram.buildProgram();
	renderProgram.addUniform("particleColor", UniformType::VEC3, &particleColor[0]);
	renderProgram.addUniform("view", UniformType::MAT4,&view[0][0]);
	renderProgram.addUniform("projection", UniformType::MAT4,&projection[0][0]);
}

void ParticleSimulationDemo::update()
{
	GAME_TIMER.stop();
	deltaTime = GAME_TIMER.delta();
	camera.update();
	view = camera.getViewMatrix();
	repaint();
}
void ParticleSimulationDemo::draw() 
{
	GAME_TIMER.restart();

	computeProgram.updateUniforms();
	GL->glDispatchCompute(totalParticles/1000,1,10);
	GL->glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	renderProgram.updateUniforms();
	GL->glPointSize(1.0f);
	GL->glBindVertexArray(particleArrayBuffer);
	GL->glDrawArrays(GL_POINTS,0,totalParticles);
}


void ParticleSimulationDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}
