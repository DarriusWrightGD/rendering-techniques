#pragma once
#include <QtCore\qelapsedtimer.h>

#define GAME_TIMER GameTimer::getInstance()
class GameTimer
{
public:
	~GameTimer(void){}
	inline float delta()
	{
		return  1.0f/elapsedTime;
	}
	inline void stop()
	{
		elapsedTime = deltaTimer.elapsed();
	}
	inline void restart()
	{
		deltaTimer.start();
	}
	static GameTimer & getInstance()
	{
		static GameTimer gameTimer;
		return gameTimer;
	}



private:
	QElapsedTimer deltaTimer;
	GameTimer(void) : elapsedTime(0.0f)
	{
		deltaTimer.start();
	}
	GameTimer(GameTimer const &) ;
	void operator=(GameTimer const &);
	float elapsedTime;


};

