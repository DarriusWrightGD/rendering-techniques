#pragma once
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglfunctions.h>
#include <QtWidgets\qopenglwidget.h>
#include <QtOpenGLExtensions\qopenglextensions.h>
#include <QtGui\qopenglfunctions_4_3_core.h>
#include <QtGui\qopenglfunctions_4_0_core.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
#include <QtCore\qtimer.h>

#include "Camera.h"
#include <QtGui\qevent.h>
#include "GameTimer.h"
#include <assimp\scene.h>

class OpenGLWindow :  public QOpenGLWidget , protected QOpenGLFunctions_4_3_Core
{
public:
	OpenGLWindow(void);
	~OpenGLWindow(void);

protected:
	void initializeGL()override;
	void paintGL()override;
	void resizeGL(int width, int height)override;
	void glUpdate();
	void keyPressEvent(QKeyEvent * e) override;
	void keyReleaseEvent(QKeyEvent * e)override;

private:
	void loadModel(const char * modelName);
	glm::mat4 getPerspectiveMatrix();
	
	void printSceneInfo(const aiScene * scene);
	void setUpFormat();
	void readShaderProgram();
	void compileShader(GLuint & shader, const char * source);
	void printShaderInfo(GLuint shader);
	std::string fileToString(const char * filename);

	GLuint points_vbo;
	GLuint colors_vbo;
	GLuint vao;
	GLuint shaderProgram;
	glm::mat4 modelMatrix;
	GLint modelLocation;
	GLint viewLocation;
	GLint projectionLocation;
	GLfloat speed;
	GLfloat lastPosition;
	
	QTimer timer;

	Camera camera;
	glm::mat4 view;
	glm::mat4 projection;
	int numberOfVertices;
};

