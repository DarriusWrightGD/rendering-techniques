#pragma once
#include <Window\OpenGLWindow.h>
#include <assimp\scene.h>
#include <Camera\FirstPersonCamera.h>
#include <QtOpenGL\qgl.h>
#include <QtOpenGL\qglcolormap.h>
#include <QtGui\qopengltexture.h>
#include <map>
#include <GameObjects\GameObject.h>
#include <Components\GuiComponent.h>
#include <Components\RenderComponent.h>
#include <Components\TransformComponent.h>
// Start the concept of the model, material, and uniforms  
/*
the model knows the number of vertices that it has, also its arraybuffer 
the material will know its uniforms and the information that it points to float *
the uniform will hold the index that the uniform is located at
*/

class ParticleSimulationDemo : public OpenGLWindow
{
public:
	ParticleSimulationDemo(void);
	~ParticleSimulationDemo(void);

	virtual void initialize()override;
	virtual void update()override;
	virtual void draw() override;
	virtual void resize(int width, int height)override;
	virtual void keyPressEvent(QKeyEvent * e)override;
	
private:
	void printComputeInfo();
	FirstPersonCamera camera;
	QTimer timer;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat4 modelView;
	uint totalParticles;
	std::vector<glm::vec4> initialPositions;
	std::vector<glm::vec4> velocities;
	GLuint particleArrayBuffer;
	glm::vec3 numberOfParticles;
	Program computeProgram;
	Program renderProgram;

	glm::vec3 blackHolePos1;
	glm::vec3 blackHolePos2;
	glm::vec3 particleColor;
	GLfloat deltaTime;

};