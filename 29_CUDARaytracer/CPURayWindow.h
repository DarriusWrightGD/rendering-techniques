#pragma once
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qlayout.h>
#include <QtWidgets\qlabel.h>
#include <QtCore\qtimer.h>
#include <QtCore\qelapsedtimer.h>
#include <Window\OpenGLWindow.h>
#include <QtGui\qevent.h>
#include <Primitives\Sphere.h>
class CPURayWindow : public QWidget
{
public:
	CPURayWindow(int width, int height);
	~CPURayWindow(void);
	void update();
	void keyReleaseEvent(QKeyEvent * e);
private:
	QLabel label;
	QHBoxLayout * layout;
	QTimer timer;
	int width;
	int height;
	Sphere sphere;
};

