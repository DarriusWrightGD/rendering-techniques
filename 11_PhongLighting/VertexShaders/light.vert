#version 430

in layout (location = 0) vec3 vertexPosition;
in layout (location = 1) vec3 vertexNormal;
in layout (location = 2) vec2 vertexTexture;

uniform vec3 lightPosition;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 modelView;
uniform mat3 normalMatrix;
out vec3 fragNormal;
out vec3 fragPosition;
struct Light
{
	vec3 Ls;
	vec3 Ld;
	vec3 La;
	vec3 position;
};

struct Material
{
	vec3 Ks;
	vec3 Kd;
	vec3 Ka;
};

uniform Light light;
uniform Material material;
out vec4 fColor;
void main()
{
	fragPosition = vec3( modelView * vec4(vertexPosition,1.0));
	fragNormal = normalize(normalMatrix *  vertexNormal);
	gl_Position = projection *  vec4(fragPosition, 1.0);
}
