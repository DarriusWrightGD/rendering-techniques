﻿#pragma once
#include <Window/OpenGLWindow.h>
#include <Camera/FirstPersonCamera.h>
#include <glm/mat4x4.hpp>
#include <GameObjects/GameObject.h>
#include <functional>
#include "Queue.h"

class RenderAppGLWindow : public OpenGLWindow
{
public:
	RenderAppGLWindow();
	virtual ~RenderAppGLWindow();
	void initialize() override;
	void update() override;
	void draw() override;
	void resize(int width, int height) override;

	void addModel(std::string fileName);
	void setColor(const glm::vec3 & newColor);
	void setColor(float x, float y, float z);

private:
	FirstPersonCamera camera;
	//Queue<std::function<void()>> glCalls;
	std::queue<std::function<void()>> glCalls;
	glm::mat4 projection;
	glm::mat4 view;
	std::vector<GameObject*> gameObjects;
	glm::vec3 backgroundColor;
};
