#include "TransformComponent.h"
#include <glm/gtx/transform.hpp>

RTTI_DEFINITIONS(TransformComponent);

TransformComponent::TransformComponent(GameObject * gameObject) : Component(gameObject), scale(1,1,1)
{
	calculateTransform();
}


TransformComponent::~TransformComponent(void)
{
}

 glm::mat4 & TransformComponent::getTransform()
{
	return modelMatrix;
}
 glm::vec3 & TransformComponent::getPosition()
{
	return position;
}
 glm::vec3 & TransformComponent::getScale()
{
	return scale;
}
 glm::vec3 & TransformComponent::getRotation()
{
	return rotation;
}

void TransformComponent::setPosition(const glm::vec3 & position)
{
	this->position = position;
	calculateTransform();
}
void TransformComponent::setScale( const glm::vec3 & scale)
{
	this->scale = scale;
	calculateTransform();
}
void TransformComponent::setRotation(const glm::vec3 & rotation)
{
	this->rotation = rotation;
	calculateTransform();
}

void TransformComponent::calculateTransform()
{
	//modelMatrix = glm::scale(scale) * glm::rotate(1.0f,glm::vec3(0,0,rotation.z)) * glm::rotate(1.0f,glm::vec3(0,rotation.y, 0)) * glm::rotate(1.0f,glm::vec3(rotation.x, 0,0)) * glm::translate(position);
	modelMatrix = glm::scale(scale)  * glm::translate(position);
}