#include "OpenGLWindow.h"

GLfloat points [] =  {0.0f,0.5f,0.0f,0.5f,-0.5f,0.0f,-0.5f,-0.5f,0.0f};


OpenGLWindow::OpenGLWindow() 
{
	setUpFormat();
}


OpenGLWindow::~OpenGLWindow(void) 
{

}


void OpenGLWindow::initializeGL()
{

	initializeOpenGLFunctions();
	glClearColor(0.4f,0.3f,0.2f,1.0f);


}

void OpenGLWindow::resizeGL(int w, int h)
{

}

void OpenGLWindow::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void OpenGLWindow::setUpFormat()
{
	QSurfaceFormat format;
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setVersion(4, 3);
	format.setProfile(QSurfaceFormat::CoreProfile);
	setFormat(format); 
}