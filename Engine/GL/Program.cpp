#include "Program.h"
#include <fstream>


Program::Program(void) : programId(0)
{
}


Program::~Program(void)
{
	if(programId)
		GL->glDeleteProgram(programId);
}

void Program::updateUniforms()
{
	GL->glUseProgram(programId);
	
	for(auto pair : uniforms)
	{
		int id = pair.first.getId();
		if( id != -1)	
		{
			setUniform(pair.first,pair.second,1,false);
		}
	}

	if(textures.size() > 0)
	{
		for (int i = textures.size()-1; i >= 0 ; i--)
		{
			GL->glActiveTexture(GL_TEXTURE0 + i);
			GL->glBindTexture(GL_TEXTURE_2D, textures[i]);
		}
	}
}

void Program::addUniform(const char * name,UniformType type, float * data)
{
	GLuint location = GL->glGetUniformLocation(programId, name);
	uniforms.insert(std::pair<Uniform,float*>(Uniform(location, type), data));
}


void Program::addTexture(const char * imagePath)
{
	QImage textureImage = QGLWidget::convertToGLFormat(QImage(imagePath));
	GLuint textureId;
	GL->glGenTextures(1,&textureId);
	GL->glActiveTexture(GL_TEXTURE0 + textures.size());
	GL->glBindTexture(GL_TEXTURE_2D,textureId);
	GL->glTexImage2D(GL_TEXTURE_2D, 0 , GL_RGBA, 
		textureImage.width(), textureImage.height(),0,
		GL_RGBA,GL_UNSIGNED_BYTE,textureImage.bits());
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	GL->glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	textures.push_back(textureId);
}

void Program::addTexture(GLuint textureId)
{
	textures.push_back(textureId);
}

void Program::addShaderFile(const std::string & filename, GLenum type)
{
	addShaderSource(std::string(std::istreambuf_iterator<char>(std::ifstream(filename)),std::istreambuf_iterator<char>()), type);
}
void Program::addShaderSource(const std::string & source, GLenum type)
{
	int params = -1;
	GLuint shader = GL->glCreateShader(type);
	const char * content = source.c_str();
	GL->glShaderSource(shader, 1, &content, NULL);
	GL->glCompileShader(shader);
	GL->glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if(GL_TRUE != params)
	{
		fprintf(stderr, "Error : GL shader index %i did not compile\n", shader);
		printShaderInfo(shader);
		system("pause");
		exit(-1);
	}

	shaders.push_back(shader);
}

void Program::printShaderInfo(GLuint shader)
{
	int maxLength = 2048;
	int actualLength = 0;
	char log[2048];
	GL->glGetShaderInfoLog(shader, maxLength,&actualLength, log);
	printf("Shader info log for GL index %u : \n%s\n", shader, log);
}
void Program::buildProgram()
{
	if(programId)
	{
		GL->glDeleteProgram(programId);
	}

	programId = GL->glCreateProgram();
	for(auto shader : shaders)
	{
		GL->glAttachShader(programId,shader);
	}
	GL->glLinkProgram(programId);

	//after linking it is good to detach shaders
	for(auto shader : shaders)
	{
		GL->glDeleteShader(shader);
	}
}

GLuint Program::getProgram()
{
	return programId;
}

void Program::setUniform(Uniform uniform, float * value, int count ,bool transpose)
{
	switch(uniform.getType())
	{
	case MAT4:
		GL->glUniformMatrix4fv(uniform.getId(),count,transpose, value);
		break;
	case MAT3:
		GL->glUniformMatrix3fv(uniform.getId(),count,transpose, value);
		break;
	case VEC3:
		GL->glUniform3fv(uniform.getId(),count,value);
		break;
	case VEC4:
		GL->glUniform4fv(uniform.getId(),count,value);
		break;
	case FLT:
		GL->glUniform1fv(uniform.getId(),count,value);
		break;
	}
}