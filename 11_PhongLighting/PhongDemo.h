#pragma once
#include <Window\OpenGLWindow.h>
#include <assimp\scene.h>
#include <Camera\FirstPersonCamera.h>
class PhongDemo : public OpenGLWindow
{
public:
	PhongDemo(void);
	~PhongDemo(void);

	virtual void initialize()override;
	virtual void update()override;
	virtual void draw() override;
	virtual void resize(int width, int height)override;

private:
	void loadModel(const char * modelName);
	glm::mat4 getPerspectiveMatrix();

	void printSceneInfo(const aiScene * scene);
	void readShaderProgram();
	void compileShader(GLuint & shader, const char * source);
	void printShaderInfo(GLuint shader);
	std::string fileToString(const char * filename);

	GLuint points_vbo;
	GLuint colors_vbo;
	GLuint vao;
	GLuint shaderProgram;
	glm::mat4 modelMatrix;
	GLint modelLocation;
	GLint viewLocation;
	GLint modelViewLocation;
	GLint normalLocation;
	GLint projectionLocation;
	GLint lightPositionLocation;
	GLint lightLsLocation;
	GLint lightLdLocation;
	GLint lightLaLocation;
	GLint modelMsLocation;
	GLint modelMdLocation;
	GLint modelMaLocation;

	GLfloat speed;
	GLfloat lastPosition;

	glm::vec3 lightLs;
	glm::vec3 lightLa;
	glm::vec3 lightLd;

	glm::vec3 modelMs;
	glm::vec3 modelMa;
	glm::vec3 modelMd;

	QTimer timer;

	FirstPersonCamera camera;
	glm::vec3 lightPosition;
	glm::mat4 view;
	glm::mat4 projection;
	glm::mat4 modelView;
	glm::mat3 normalMatrix;
	int numberOfVertices;

	

};

