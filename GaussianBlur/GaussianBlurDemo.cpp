#include "GaussianBlurDemo.h"
#include <QtCore\qdebug.h>
#include <iostream>
#include <fstream>
#include <ostream>
#include <assimp\cimport.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>

#include <stdlib.h>
#include <GLContext.h>

float fullScreenPos [] = 
{
	-1,-1,0,
	1,-1,0,
	1,1,0,

	1,1,0,
	-1,1,0,
	-1,-1,0,
};


float depthQuadPos [] = 
{	
	0,-1,0,
	1,-1,0,
	1,1,0,

	1,1,0,
	0,1,0,
	0,-1,0,
};

float depthQuadTex [] = 
{
	0.0, 0.0,
	1.0, 0.0,
	1.0, 1.0,
	1.0, 1.0,
	0.0, 1.0,
	0.0, 0.0,
};

float colorQuadPos [] = 
{

	-1,-1,0,
	0,-1,0,
	0,1,0,

	0,1,0,
	-1,1,0,
	-1,-1,0,

};

float colorQuadTex [] = 
{
	0.0, 0.0,
	1.0, 0.0,
	1.0, 1.0,
	1.0, 1.0,
	0.0, 1.0,
	0.0, 0.0,
};


GaussianBlurDemo::GaussianBlurDemo(void): lightPosition(glm::vec4(glm::normalize(glm::vec3(.2,.2,-1)),0.0f)),
	lightColor(0.4f,0.4f,0.9f),diffuse(0.2f,0.2f,0.9f,1.0f), specular(0.9f,0.0f,0.0f, 1.0f), ambientColor(0.3f,0.1,0.2), camera((float)width()/height())
{
	camera.initialize();
	camera.setPosition(glm::vec3(0,0,2));
	camera.setMouse(mouse);
	camera.setKeyboard(keyboard);

}


GaussianBlurDemo::~GaussianBlurDemo(void)
{
	delete colorGuiComponent;
	//delete depthGuiComponent;
	delete cubeRenderComponent;
	delete monkeyRenderComponent;
	delete cubeObject;
	delete monkeyObject;
}

void GaussianBlurDemo::keyPressEvent(QKeyEvent * e)
{
	if((e->key() == Qt::Key_F10))
	{
		unsigned char * buffer = new unsigned char[width() * height() * 3];
		GL->glReadPixels(0,0,width(), height(),GL_RGB,GL_UNSIGNED_BYTE,buffer);
		QImage image(buffer,width(),height(),QImage::Format::Format_RGB888);
		image = image.mirrored();
		image.save("glSaved.jpg");
		delete [] buffer;

	}
}

void GaussianBlurDemo::initialize()
{
	cubeObject = new GameObject();
	cubeObject->getTransform()->setPosition(glm::vec3(-3,0,-10));
	cubeRenderComponent = new RenderComponent(cubeObject,"../Models/cube.obj");
	cubeObject->add(cubeRenderComponent);

	monkeyObject = new GameObject();
	monkeyObject->setPosition(glm::vec3(2,0,-13));
	monkeyRenderComponent = new RenderComponent(monkeyObject, "../Models/monkey.obj");
	cubeObject->add(monkeyRenderComponent);

	cubeRenderComponent->addShaderFile("VertexShaders/texture.vert", GL_VERTEX_SHADER);
	cubeRenderComponent->addShaderFile("FragmentShaders/texture.frag", GL_FRAGMENT_SHADER);
	cubeRenderComponent->buildProgram();



	monkeyRenderComponent->addShaderFile("VertexShaders/light.vert", GL_VERTEX_SHADER);
	monkeyRenderComponent->addShaderFile("FragmentShaders/light.frag", GL_FRAGMENT_SHADER);
	monkeyRenderComponent->buildProgram();

	monkeyRenderComponent->addUniform("model", MAT4, &monkeyObject->getTransform()->getTransform()[0][0]);
	monkeyRenderComponent->addUniform("view", MAT4, &view[0][0]);
	monkeyRenderComponent->addUniform("normalMatrix",MAT3,&normalMatrix[0][0]);
	monkeyRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	monkeyRenderComponent->addUniform("light.color",VEC3,&lightColor[0]);
	monkeyRenderComponent->addUniform("material.diffuse",VEC4,&diffuse[0]);
	monkeyRenderComponent->addUniform("material.specular",VEC4,&specular[0]);
	monkeyRenderComponent->addUniform("ambient",VEC3,&ambientColor[0]);
	monkeyRenderComponent->addUniform("light.position",VEC4,&lightPosition[0]);

	cubeRenderComponent->addUniform("model", MAT4, &cubeObject->getTransform()->getTransform()[0][0]);
	cubeRenderComponent->addUniform("view", MAT4, &view[0][0]);
	cubeRenderComponent->addUniform("projection",MAT4,&projection[0][0]);
	cubeRenderComponent->addTexture("../Images/brick.png");

	GL->glGenBuffers(1, &cameraBuffer);
	GL->glBindBuffer(GL_UNIFORM_BUFFER,cameraBuffer);
	GL->glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * 2, NULL, GL_DYNAMIC_DRAW);

	int blockId = 0;
	monkeyUniformBlockIndex = GL->glGetUniformBlockIndex(monkeyRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(monkeyRenderComponent->getProgramId(), monkeyUniformBlockIndex,blockId);    
	boxUniformBlockIndex = GL->glGetUniformBlockIndex(cubeRenderComponent->getProgramId(),"cameraBlock");
	GL->glUniformBlockBinding(cubeRenderComponent->getProgramId(), boxUniformBlockIndex,blockId);    

	GL->glBindBufferBase(GL_UNIFORM_BUFFER, blockId, cameraBuffer);
	float * cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&projection[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);

	//creating a renderbuffer
	//GL->glGenRenderbuffers(1,&colorBuffer);
	//GL->glGenRenderbuffers(1,&depthBuffer);

	//GL->glBindRenderbuffer(GL_RENDERBUFFER, colorBuffer);
	//GL->glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA,256,256);

	//GL->glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	//GL->glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT24, 256,256);


	//GL->glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER,colorBuffer);
	//GL->glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER,depthBuffer);

	GL->glGenFramebuffers(1,&frameBuffer);
	GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffer);

	GL->glGenTextures(1,&depthBuffer);
	GL->glBindTexture(GL_TEXTURE_2D, depthBuffer);
	GL->glActiveTexture(GL_TEXTURE0);
	GL->glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32, width(),height());

	GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	GL->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,GL_TEXTURE_2D,depthBuffer,0);

	GLenum drawBuffers[] = {GL_NONE};
	GL->glDrawBuffers(1,drawBuffers);
	
	GL->glGenFramebuffers(1,&secondaryFrameBuffer);
	GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffer);

	GL->glGenTextures(1,&colorBuffer);
	GL->glActiveTexture(GL_TEXTURE0);
	GL->glBindTexture(GL_TEXTURE_2D, colorBuffer);
	GL->glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width(),height());
	GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	GL->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	GL->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,colorBuffer,0);

	GLenum secondaryDrawBuffers[] = {GL_COLOR_ATTACHMENT0};
	GL->glDrawBuffers(1,secondaryDrawBuffers);

	GL->glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebufferObject());

	

	screenObject = new GameObject();

	colorGuiComponent = new GuiComponent(screenObject,colorBuffer,glm::vec2(0.0f,0.0f),glm::vec2(1.0f,1.0f));
	colorGuiComponent->addShaderFile("VertexShaders/post.vert", GL_VERTEX_SHADER);
	colorGuiComponent->addShaderFile("FragmentShaders/post.frag", GL_FRAGMENT_SHADER);
	colorGuiComponent->buildProgram();
	postProcessLocation = GL->glGetSubroutineUniformLocation(colorGuiComponent->getProgramId(),GL_FRAGMENT_SHADER,"postEffect");
	edgeDetectionIndex = GL->glGetSubroutineIndex(colorGuiComponent->getProgramId(),GL_FRAGMENT_SHADER,"edgeDetection");
	GL->glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &edgeDetectionIndex);
	screenObject->add(colorGuiComponent);
}

void GaussianBlurDemo::update()
{
	GAME_TIMER.stop();
	GLfloat deltaTime = GAME_TIMER.delta();

	//GL->glUniform1i(textureLocation,0);

	const float lightSpeed = 2.0f;

	if(GetAsyncKeyState('J'))
	{
		lightPosition.x -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('L'))
	{
		lightPosition.x += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('P'))
	{
		lightPosition.z -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('O'))
	{
		lightPosition.z += lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('K'))
	{
		lightPosition.y -= lightSpeed * GAME_TIMER.delta();
	}
	if(GetAsyncKeyState('I'))
	{
		lightPosition.y += lightSpeed * GAME_TIMER.delta();
	}

	view = camera.getViewMatrix();
	camera.update();

	lightPosition = glm::vec4(glm::normalize(glm::vec3(lightPosition)),0.0);
	cubeObject->update();
	monkeyObject->update();
	screenObject->update();


	repaint();
}
void GaussianBlurDemo::draw() 
{
	GAME_TIMER.restart();

	GL->glBindBufferBase(GL_UNIFORM_BUFFER, 0, cameraBuffer);
	float * cameraUboPtr = (float*)GL->glMapBufferRange(GL_UNIFORM_BUFFER,0, sizeof(float)*32, GL_MAP_WRITE_BIT| GL_MAP_INVALIDATE_BUFFER_BIT);
	memcpy(&cameraUboPtr[0],&projection[0][0],sizeof(float) * 16);
	memcpy(&cameraUboPtr[16],&view[0][0],sizeof(float) * 16);
	GL->glUnmapBuffer(GL_UNIFORM_BUFFER);

	GL->glBindFramebuffer(GL_FRAMEBUFFER,frameBuffer);
	GL->glViewport(0,0,width(),height());
	GL->glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	cubeObject->draw();
	monkeyObject->draw();

	GL->glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebufferObject());
	GL->glViewport(0,0,width(),height());
	GL->glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	//colorRenderComponent->draw();
	screenObject->draw();
	//cubeObject->draw();
	//monkeyObject->draw();
}
void GaussianBlurDemo::resize(int width, int height)
{
	GL->glViewport(0,0,width,height);
	camera.setAspectRatio((float)width/height);
	projection = camera.getProjectionMatrix();
}
